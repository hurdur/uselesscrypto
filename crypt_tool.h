/* Michael Bombardieri places this file in the public domain. */

#ifndef BT_CRYPT_TOOL_H
#define BT_CRYPT_TOOL_H 1

#define ROUNDS_MIN 16
#define ROUNDS_MAX 250000

#define ALGNAME_BUFSZ 8

struct opt_final {
	int encrypt;
	int add_salt;
	char *keyfile;
	char *infile;
	char *outfile;
};

struct key_params {
	char alg[ALGNAME_BUFSZ];
	int rounds;
	u_int32_t *inkey;
	int inkey_len;
	u_int32_t *iv;
	int iv_len;
};

struct key_params *read_keyfile(const char *);
struct opt_final *local_getopt(int, char **);
int local_strtonum(const char *, int, int);
void local_strlcat(char *, const char *, size_t);
void *getrandom(int);

#endif
