/* Michael Bombardieri places this file in the public domain. */

#include <sys/types.h>

#include <string.h>

#include "crypt_lib.h"

static u_int8_t key_iv[256] = {
	0x48, 0x66, 0x99, 0xA4, 0x0A, 0x58, 0x62, 0x23, 
	0xC3, 0xEE, 0x95, 0x9A, 0xC8, 0x79, 0xDB, 0x70, 
	0x86, 0xC2, 0x70, 0x7F, 0x89, 0x7C, 0x9F, 0x17, 
	0x12, 0x70, 0x26, 0x05, 0x0F, 0x38, 0x00, 0xE8, 
	0xFB, 0x87, 0x6F, 0x0B, 0xEA, 0xA5, 0x71, 0xA7, 
	0x26, 0x0D, 0xD9, 0xDE, 0x27, 0x62, 0x1D, 0x2A, 
	0x83, 0x92, 0xDC, 0xC4, 0x5B, 0xC3, 0x2C, 0xB7, 
	0xFA, 0x0F, 0x0A, 0x47, 0xB4, 0x1E, 0xFC, 0x18, 
	0x61, 0x5D, 0x03, 0x01, 0x4D, 0x16, 0xD4, 0x6A, 
	0xD4, 0xFE, 0x19, 0x5F, 0x32, 0xFA, 0x0B, 0xEC, 
	0x1E, 0xE9, 0x5B, 0x73, 0xE3, 0xC2, 0x98, 0xE7, 
	0x5A, 0xAD, 0x07, 0x93, 0xAB, 0x60, 0xD5, 0xA4, 
	0x04, 0x45, 0x9E, 0xD8, 0xCD, 0x9C, 0xC2, 0x6B, 
	0xD4, 0xEE, 0x1C, 0x38, 0x34, 0x2E, 0x0C, 0x7E, 
	0x38, 0x8F, 0x6A, 0x8B, 0xCD, 0x29, 0xF7, 0x54, 
	0xB2, 0x52, 0x50, 0x17, 0x37, 0x8C, 0x9D, 0x92, 
	0x16, 0x0A, 0x85, 0xA8, 0x38, 0x8D, 0x10, 0x61, 
	0xDA, 0xD2, 0x99, 0xDE, 0x1A, 0xCB, 0x1D, 0xBB, 
	0x6B, 0x38, 0xC5, 0x77, 0x04, 0xE9, 0x26, 0xFC, 
	0xDE, 0xDF, 0x99, 0x93, 0xF6, 0xF8, 0x02, 0xF6, 
	0x3A, 0x4F, 0x37, 0xAF, 0xC9, 0xE8, 0xCF, 0x32, 
	0xF1, 0x2F, 0x12, 0xEF, 0xCB, 0x7E, 0xF9, 0xDA, 
	0x74, 0x9A, 0xFF, 0x07, 0x60, 0x69, 0x88, 0xB4, 
	0xE9, 0x10, 0x33, 0x95, 0xD0, 0x37, 0x5C, 0x9B, 
	0x84, 0x1B, 0x42, 0x54, 0xAF, 0x6C, 0xF5, 0xD3, 
	0x7A, 0x02, 0xB6, 0xE6, 0x41, 0x8E, 0xDA, 0x25, 
	0x20, 0xFC, 0xDC, 0xB4, 0x26, 0xE0, 0x3D, 0xDE, 
	0x98, 0x54, 0x8F, 0x49, 0x05, 0xDF, 0x25, 0x2E, 
	0x06, 0x74, 0x7A, 0x0D, 0x5A, 0x54, 0xFE, 0x01, 
	0xA2, 0x53, 0x3B, 0xF2, 0xDF, 0x0B, 0xD9, 0xCF, 
	0x5D, 0x74, 0x2D, 0xC5, 0x86, 0x77, 0x81, 0x36, 
	0xBD, 0xB4, 0xE3, 0x54, 0x1E, 0xD6, 0x52, 0xB2
};

char *
bt_cipher_name(void)
{
	static char name[] = "mbc2";
	return name;
}

int
bt_cipher_block(void)
{
	return 8;
}

int
bt_cipher_key(void)
{
	return 16;
}

int
bt_cipher_rounds(void)
{
	return 16;
}

void
bt_cipher_prepare(void *in_key, struct ci_key *key)
{
	u_int32_t in_word[4];
	u_int8_t keystream[588];
	u_int8_t blk[16];
	u_int8_t *f;
	int i, j, ki;
	u_int8_t t;

	/* in_key taken to be network byte order */
	memcpy(in_word, in_key, sizeof(in_word));
	for (i = 0; i < 4; i++)
		in_word[i] = ntohl(in_word[i]);
	f = (u_int8_t *) in_word;

	for (i = 0; i < 588; i++)
		keystream[i] = (u_int8_t)(i & 255);

	blk[0] = (u_int8_t)(f[0] ^ f[1]);
	blk[1] = (u_int8_t)(f[0] ^ f[2]);
	blk[2] = (u_int8_t)(f[0] ^ f[3]);
	blk[3] = (u_int8_t)(f[1] ^ f[2]);
	blk[4] = (u_int8_t)(f[1] ^ f[3]);
	blk[5] = (u_int8_t)(f[2] ^ f[3]);
	blk[6] = f[0];
	blk[7] = f[1];
	blk[8] = f[2];
	blk[9] = f[3];

	f += 4;
	blk[10] = (u_int8_t)(blk[0] ^ f[0]);
	blk[11] = (u_int8_t)(blk[1] ^ f[1]);
	blk[12] = (u_int8_t)(blk[2] ^ f[2]);
	blk[13] = (u_int8_t)(blk[3] ^ f[3]);
	blk[14] = (u_int8_t)(f[0] ^ f[1]);
	blk[15] = (u_int8_t)(f[2] ^ f[3]);

	for (i = 0; i < 588; i++) {
		keystream[i] = (u_int8_t)((keystream[i] ^ (blk[i & 15] + f[i & 3])) & 255);
		f[i & 3] = keystream[i];
	}

	blk[0] = (u_int8_t)(f[0] ^ f[1]);
	blk[1] = (u_int8_t)(f[0] ^ f[2]);
	blk[2] = (u_int8_t)(f[0] ^ f[3]);
	blk[3] = (u_int8_t)(f[1] ^ f[2]);
	blk[4] = (u_int8_t)(f[1] ^ f[3]);
	blk[5] = (u_int8_t)(f[2] ^ f[3]);

	f += 4;
	blk[6] = (u_int8_t)(f[0] ^ blk[2]);
	blk[7] = (u_int8_t)(f[1] ^ blk[3]);
	blk[8] = (u_int8_t)(f[2] ^ blk[4]);
	blk[9] = (u_int8_t)(f[3] ^ blk[5]);

	f += 4;
	blk[10] = (u_int8_t)(f[0] ^ blk[6]);
	blk[11] = (u_int8_t)(f[1] ^ blk[7]);
	blk[12] = (u_int8_t)(f[2] ^ blk[8]);
	blk[13] = (u_int8_t)(f[3] ^ blk[9]);
	blk[14] = (u_int8_t)(f[0] ^ f[2]);
	blk[15] = (u_int8_t)(f[1] ^ f[3]);

	f[1] = (u_int8_t)(f[1] ^ f[0]);
	f[2] = (u_int8_t)(f[2] ^ f[0]);
	f[3] = (u_int8_t)(f[3] ^ f[0]);

	for (i = 0; i < 588; i++) {
		keystream[i] = (u_int8_t)((keystream[i] + (blk[i & 15] ^ f[i & 3])) & 255);
		f[i & 3] = keystream[i];
	}

	for (i = 0; i < 16; i++) {
		ki = 36 * i;
		for (j = 0; j < 48; j++) {
			t = (u_int8_t)(key_iv[(int)(keystream[ki + j])]);
			t = (u_int8_t)(t ^ keystream[ki + j]);
			key->sb[i][j] = (u_int8_t)((t & 1) ^ ((t & 2) >> 1));
			key->pm[i][j] = (u_int8_t)(t >> 2);
		}
		for (j = 0; j < 16; j++) {
			key->pm[i][j + 48] = (u_int8_t)((key->pm[i][j] + 
				key->pm[i][j + 16]) & 63);
		}
	}

	memset(in_word, 0, sizeof(in_word));
	memset(blk, 0, sizeof(blk));
	memset(keystream, 0, sizeof(keystream));
	i = j = ki = t = 0;
}

void
bt_cipher_encode(void *in_blk, struct ci_key *key, int rounds)
{
	u_int8_t blk[64];
	u_int32_t in_word[2];
	int bi, i, j, k;
	u_int8_t pc, t;
	u_int8_t *in;

	/* in_blk taken to be network byte order */
	memcpy(in_word, in_blk, sizeof(in_word));
	in_word[0] = ntohl(in_word[0]);
	in_word[1] = ntohl(in_word[1]);
	in = (u_int8_t *) in_word;

	for (i = 0; i < 8; i++)
		for (j = 0; j < 8; j++) {
			k = (i << 3) + j;
			blk[k] = (u_int8_t)((in[i] >> j) & 1);
		}

	for (i = 0; i < rounds; i++) {
		k = i & 15;

		for (j = 48; j < 64; j++)
			blk[j] = (u_int8_t)(blk[j] ^ blk[j - 36]);

		pc = (u_int8_t)((i+1) & 1);
		for (j = 0; j < 48; j++) {
			t = key->sb[k][j];
			blk[j] = (u_int8_t)(blk[j] ^ ((pc + t) & 1));
			pc = blk[j];
		}

		for (j = 0; j < 64; j++) {
			bi = (int) key->pm[k][j];
			SWAP(&blk[j], &blk[bi]);
		}
	}

	memset(in, 0, 8);
	for (i = 0; i < 8; i++) {
		for (j = 0; j < 8; j++) {
			k = (i << 3) + j;
			in[i] |= blk[k] << j;
		}
	}

	in_word[0] = htonl(in_word[0]);
	in_word[1] = htonl(in_word[1]);
	memcpy(in_blk, in_word, sizeof(in_word));

	memset(blk, 0, sizeof(blk));
	memset(in_word, 0, sizeof(in_word));
	bi = i = j = k = pc = t = 0;
}

#ifdef ECB
void
bt_cipher_decode(void *in_blk, struct ci_key *key, int rounds)
{
	u_int8_t blk[64];
	u_int32_t in_word[2];
	int bi, i, j, k;
	u_int8_t pc, t;
	u_int8_t *in;

	/* in_blk taken to be network byte order */
	memcpy(in_word, in_blk, sizeof(in_word));
	in_word[0] = ntohl(in_word[0]);
	in_word[1] = ntohl(in_word[1]);
	in = (u_int8_t *) in_word;

	for (i = 0; i < 8; i++)
		for (j = 0; j < 8; j++) {
			k = (i << 3) + j;
			blk[k] = (u_int8_t)((in[i] >> j) & 1);
		}

	for (i = --rounds; i >= 0; i--) {
		k = i & 15;

		for (j = 63; j >= 0; j--) {
			bi = (int) key->pm[k][j];
			SWAP(&blk[j], &blk[bi]);
		}

		for (j = 47; j > 0; j--) {
			t = key->sb[k][j];
			blk[j] = (u_int8_t)(blk[j] ^ ((t + blk[j-1]) & 1));
		}

		/* substitution for j=0 */
		t = key->sb[k][0];
		pc = (u_int8_t)((k+1) & 1);
		blk[0] = (u_int8_t)(blk[0] ^ ((t + pc) & 1));

		for (j = 63; j >= 48; j--)
			blk[j] = (u_int8_t)(blk[j] ^ blk[j - 36]);
	}

	memset(in, 0, 8);
	for (i = 0; i < 8; i++) {
		for (j = 0; j < 8; j++) {
			k = (i << 3) + j;
			in[i] |= blk[k] << j;
		}
	}

	in_word[0] = htonl(in_word[0]);
	in_word[1] = htonl(in_word[1]);
	memcpy(in_blk, in_word, sizeof(in_word));

	memset(blk, 0, sizeof(blk));
	memset(in_word, 0, sizeof(in_word));
	bi = i = j = k = pc = t = 0;
}
#endif /* ECB */
