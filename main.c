/* Michael Bombardieri places this file in the public domain. */

#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>

#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define SALT_BYTES 16
#define SALT_ID "zzzz"

#include "crypt_lib.h"
#include "crypt_tool.h"

static void
copy_file(char *new_name, int fd, char *path, struct stat *st,
	int encrypt, int add_salt)
{
	u_int32_t *mapped;
	u_int32_t word;
	ssize_t written;
	off_t in_words;
	void *salt;
	int new_fd;
	int i;

	new_fd = open(new_name, O_WRONLY|O_CREAT, 0644);
	if (new_fd == -1)
		err(1, "open: %s", new_name);
	mapped = mmap(NULL, st->st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (mapped == MAP_FAILED)
		err(1, "mmap: %s", path);
	if (encrypt && add_salt) {
		salt = getrandom(SALT_BYTES);
		if (salt == NULL)
			errx(1, "random data generation failed");
		written = write(new_fd, SALT_ID, sizeof(SALT_ID) - 1);
		if (written != sizeof(SALT_ID) - 1)
			err(1, "write: %s", new_name);
		written = write(new_fd, salt, SALT_BYTES);
		if (written != SALT_BYTES)
			err(1, "write: %s", new_name);
		free(salt);
	}

	in_words = st->st_size / 4;
	for (i = 0; i < in_words; i++) {
		if (encrypt)
			word = htonl(mapped[i]);
		else
			word = ntohl(mapped[i]);
		written = write(new_fd, &word, sizeof(word));
		if (written != sizeof(word))
			err(1, "write: %s", new_name);
	}
	if (close(new_fd) == -1)
		err(1, "close: %s", new_name);
	if (munmap(mapped, st->st_size) == -1)
		err(1, "munmap: %s", path);
}

static void
encode_file(char *path, struct key_params *k, int encrypt, int add_salt)
{
	struct ci_key btkey;
	u_int32_t *mapped;
	struct stat st;
	int len;
	int fd;
	int i;

	fd = open(path, O_RDWR, 0644);
	if (fd == -1)
		err(1, "open: %s", path);
	if (fstat(fd, &st) == -1)
		err(1, "stat: %s", path);
	len = (int) (st.st_size / 4);
	mapped = mmap(NULL, st.st_size, PROT_READ|PROT_WRITE,
		MAP_SHARED, fd, 0);
	if (mapped == MAP_FAILED)
		err(1, "mmap: %s", path);
	if (!encrypt) {
		if (memcmp(mapped, SALT_ID, sizeof(SALT_ID) - 1) == 0)
			add_salt = 1;
		else
			add_salt = 0;
	}
	bt_key(k->inkey, k->inkey_len * 4, &btkey);
	if (add_salt)
		bt_cfb(mapped + 1, len - 1, &btkey, k->iv, encrypt, k->rounds);
	else
		bt_cfb(mapped, len, &btkey, k->iv, encrypt, k->rounds);

	memset(k, 0, sizeof(*k));
	memset(&btkey, 0, sizeof(btkey));
	for (i = 0; i < len; i++)
		mapped[i] = ntohl(mapped[i]);
	if (!encrypt && add_salt)
		(void) memmove(mapped, &mapped[5],
			st.st_size - SALT_BYTES);
	if (msync(mapped, 0, MS_SYNC) == -1)
		err(1, "msync");
	if (munmap(mapped, st.st_size) == -1)
		err(1, "munmap");
	if (!encrypt && add_salt)
		if (ftruncate(fd, st.st_size - SALT_BYTES - sizeof(SALT_ID - 1)) == -1)
			err(1, "ftruncate");
	if (close(fd) == -1)
		err(1, "close");
}

int
main(int argc, char *argv[])
{
	struct opt_final *opt;
	struct key_params *k; 
	struct stat st;
	int in_fd;

	opt = local_getopt(argc, argv);
	k = read_keyfile(opt->keyfile);
	if (strncmp(k->alg, bt_cipher_name(), ALGNAME_BUFSZ) != 0)
		errx(1, "algorithm mismatch: want %s, got %s",
			bt_cipher_name(), k->alg);

	in_fd = open(opt->infile, O_RDONLY, 0644);
	if (in_fd == -1)
		err(1, "open: %s", opt->infile);
	if (fstat(in_fd, &st) == -1)
		err(1, "stat: %s", opt->infile);
	if (st.st_size < 4)
		errx(1, "%s: file is too short", opt->infile);
	if (st.st_size % 4 != 0) 
		errx(1, "%s: file length must be a multiple of 4", opt->infile);
	copy_file(opt->outfile, in_fd, opt->infile,
		&st, opt->encrypt, opt->add_salt);
	if (close(in_fd) == -1)
		err(1, "close: %s", opt->infile);
	encode_file(opt->outfile, k, opt->encrypt,
		opt->add_salt);
	return (0);
}
