/* Michael Bombardieri places this file in the public domain. */

#include <sys/types.h>

#include <string.h>

#include "crypt_lib.h"

static u_int32_t key_iv[64] = {
	0xFA5D7CEC, 0x734533F8, 0x484EA4B2, 0x2BF0E46B,
	0xA853884B, 0xCBE7DEE0, 0x976DF92A, 0xE2B17575,
	0xF9BC1EFD, 0xF8ADC573, 0x2E5B6EA9, 0x1C5A4FFD,
	0x3DF27762, 0x2D7BF0BD, 0x039D26D6, 0x0A937D09,
	0x01052BC2, 0x8AEEEA1C, 0xFB7D3EBE, 0x2449A182,
	0x17B71BA3, 0xC6E68955, 0x74C7A0CD, 0xE1B88745,
	0x4D71958C, 0x149CEB88, 0x5344E0B9, 0xC3F0A21F,
	0xA348429B, 0xF672C8CD, 0xD01FA55C, 0x4C6FD3DF,
	0xC05B564E, 0xE0D81861, 0xA3B3EAAC, 0x17AE4A08,
	0x9D8DF3BF, 0x1353CBFE, 0x25C0F1C8, 0xD52818EC,
	0x1BA102B0, 0xC2FDC394, 0x9AB0505E, 0xF8D5F8CB,
	0x708749DA, 0x34C5D60B, 0xD1C046B0, 0x4C8E4672,
	0x0E236A84, 0x5A36C5AB, 0xDC9B9EB5, 0x0ADB5718,
	0x57D27CC5, 0xC4ECAEFD, 0xB57C0D9E, 0x764E92EA,
	0x4CA01ED6, 0xA7314405, 0xEE5E6B4B, 0xF9560189,
	0xC50CD4D5, 0xBFE0AFD8, 0x8322F984, 0x65601B60
};

static short p[16][4] = {
	{3, 0, 1, 2},
	{3, 0, 2, 1},
	{3, 1, 0, 2},
	{3, 1, 2, 0},
	{3, 2, 0, 1},
	{3, 2, 1, 0},
	{1, 0, 2, 3},
	{1, 0, 3, 2},
	{1, 2, 0, 3},
	{1, 2, 3, 0},
	{0, 1, 2, 3},
	{0, 1, 3, 2},
	{0, 2, 1, 3},
	{0, 2, 3, 1},
	{0, 3, 1, 2},
	{0, 3, 2, 1}
};

char *
bt_cipher_name(void)
{
	static char name[] = "mbc1";
	return name;
}

int
bt_cipher_block(void)
{
	return 16;
}

int
bt_cipher_key(void)
{
	return 16;
}

int
bt_cipher_rounds(void)
{
	return 16;
}

void
bt_cipher_prepare(void *in_key, struct ci_key *key)
{
	u_int8_t key_exp[64];
	u_int32_t blk[4];
	u_int32_t fbk, t;
	u_int32_t *in;
	int i, j, x;

	in = in_key;
	fbk = MAGIC;

	blk[0] = ADD32(in[0], ROTL32(fbk, 3));
	blk[1] = ADD32(in[1], ROTL32(blk[0], 5));
	blk[2] = ADD32(in[2], ROTL32(blk[1], 7));
	blk[3] = ADD32(in[3], ROTL32(blk[2], 11));

	x = ROTL32(blk[3], 1) & 63;
	fbk ^= ADD32(ROTL32(blk[3], 2), key_iv[x]);

	blk[0] ^= ROTL32(fbk, 1);
	blk[1] ^= ROTL32(blk[0], 2);
	blk[2] ^= ROTL32(blk[1], 3);
	blk[3] ^= ROTL32(blk[2], 4);

	x = (x + (blk[0] & 255)) & 63;
	for (j = 0; j < 4; j++) {
		blk[P4] = ADD32(blk[P4], key_iv[x]);
		blk[P3] = ADD32(blk[P3], key_iv[(x + 1) & 63]); 
		blk[P2] = ADD32(blk[P2], key_iv[(x + 2) & 63]); 
		blk[P1] = ADD32(blk[P1], key_iv[(x + 3) & 63]);
	}

	key->x[0] = ROTL32(blk[0], 5); 
	key->x[1] = ROTL32(blk[1], 9); 
	key->x[2] = ROTL32(blk[2], 13); 
	key->x[3] = ROTL32(blk[3], 17);

	i = 4;
	for (j = 0; j < 7; j++) {
		blk[P1] = ADD32(blk[P1], key_iv[x]); 
		blk[P2] = ADD32(blk[P2], key_iv[(x + 1) & 63]); 
		blk[P3] = ADD32(blk[P3], key_iv[(x + 2) & 63]); 
		blk[P4] = ADD32(blk[P4], key_iv[(x + 3) & 63]);

		key->x[i] = ROTL32(blk[P1], 5); 
		key->x[i + 1] = ROTL32(blk[P2], 9); 
		key->x[i + 2] = ROTL32(blk[P3], 13); 
		key->x[i + 3] = ROTL32(blk[P4], 17);

		x = (x + 4) & 63;
		i += 4;
	}

	for (i = 0; i < 32; i++) {
		key_exp[i] = (u_int8_t)(key->x[i] & 255);
		key_exp[i + 32] = (u_int8_t)((key->x[i] & 0xFF00) >> 8);
	}

	for (i = 0; i < 64; i++) {
		x = key_exp[i] & 31;
		t = key->x[i & 31]; 
		key->x[i & 31] = key->x[x];
		key->x[x] = t;
	}

	memset(key_exp, 0, sizeof(key_exp));
	memset(blk, 0, sizeof(blk));
	fbk = t = i = j = x = 0;
}

void
bt_cipher_encode(void *in_blk, struct ci_key *key, int rounds)
{
	u_int32_t *blk;
	u_int32_t fbk;
	int i, j;

	blk = in_blk;
	for (i = 0; i < rounds; i++) {
		j = i & 15;

		blk[P1] ^= key->x[j + 11];
		blk[P2] ^= key->x[j + 13];
		blk[P3] ^= key->x[j + 15];
		blk[P4] ^= key->x[j + 16];

		blk[P1] = ADD32(blk[P1], key->x[j]);
		blk[P2] = ADD32(blk[P2], ROTL32(blk[P1], 1));
		blk[P3] = ADD32(blk[P3], ROTL32(blk[P2], 2));
		blk[P4] = ADD32(blk[P4], ROTL32(blk[P3], 3));

		fbk = ADD32(ROTL32(blk[P4], 5), key->x[j]) ^ key->x[j + 1];
		blk[P1] ^= fbk;
		blk[P2] ^= ROTL32(fbk, 1);
		blk[P3] ^= ROTL32(fbk, 2);
		blk[P4] ^= ROTL32(key->x[j], 1);
	}

	fbk = i = j = 0;
}

#ifdef ECB
void
bt_cipher_decode(void *in_blk, struct ci_key *key, int rounds)
{
	u_int32_t *blk;
	u_int32_t fbk;
	int i, j;

	blk = in_blk;
	for (i = --rounds; i >= 0; i--) {
		j = i & 15;

		blk[P4] ^= ROTL32(key->x[j], 1); 
		fbk = ADD32(ROTL32(blk[P4], 5), key->x[j]) ^ key->x[j + 1];
		blk[P1] ^= fbk; 
		blk[P2] ^= ROTL32(fbk, 1); 
		blk[P3] ^= ROTL32(fbk, 2);

		blk[P4] = SUB32(blk[P4], ROTL32(blk[P3], 3)); 
		blk[P3] = SUB32(blk[P3], ROTL32(blk[P2], 2));
		blk[P2] = SUB32(blk[P2], ROTL32(blk[P1], 1));
		blk[P1] = SUB32(blk[P1], key->x[j]); 

		blk[P1] ^= key->x[j + 11];
		blk[P2] ^= key->x[j + 13];
		blk[P3] ^= key->x[j + 15];
		blk[P4] ^= key->x[j + 16];
	}

	fbk = i = j = 0;
}
#endif /* ECB */
