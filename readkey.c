/* Michael Bombardieri places this file in the public domain. */

#include <sys/types.h>

#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "crypt_tool.h"

#define KEYFILE_BUFSZ 1024

struct key_params *
read_keyfile(const char *path)
{
	int i;
	int fd;
	int bytes;
	char *cp;
	char *sep;
	char *csv[4];
	u_int32_t *np;
	char buf[KEYFILE_BUFSZ];
	static struct key_params k;

	memset(&k, 0, sizeof(k));
	memset(buf, 0, sizeof(buf));

	fd = open(path, O_RDONLY, 0644);
	if (fd == -1)
		err(1, "open: %s", path);
	bytes = read(fd, buf, sizeof(buf));
	if (bytes <= 0)
		errx(1, "failed to read from keyfile");
	if (close(fd) == -1)
		err(1, "close: ");

	cp = strchr(buf, '\n');
	if (cp == NULL)
		errx(1, "key file invalid: line terminator not found");
	*cp = '\0';

	csv[0] = buf;
	for (i = 1; i < 4; i++) {
		sep = strchr(csv[i - 1], ':');
		if (sep == NULL)
			errx(1, "key file invalid: field terminator %d not found", i);
		*sep = '\0';
		csv[i] = sep + 1;
	}

	(void) strncpy(k.alg, csv[0], ALGNAME_BUFSZ);
	k.rounds = local_strtonum(csv[1],
		ROUNDS_MIN, ROUNDS_MAX);

	/* IV hex string */
	k.iv_len = (int) (strlen(csv[2]) / 8);
	k.iv = calloc(k.iv_len, 4);
	if (k.iv == NULL)
		err(1, "failed to allocate IV");
	np = k.iv;
	for (cp = csv[2]; *cp; cp += 8)
		if (!sscanf(cp, "%08lx", (long unsigned int *)np++))
			errx(1, "sscanf: failed to read hex string");

	/* key stream hex string */
	k.inkey_len = (int) (strlen(csv[3]) / 8);
	k.inkey = calloc(k.inkey_len, 4);
	if (k.iv == NULL)
		err(1, "failed to allocate key stream");
	np = k.inkey;
	for (cp = csv[3]; *cp; cp += 8)
		if (!sscanf(cp, "%08lx", (long unsigned int *)np++))
			errx(1, "sscanf: failed to read hex string");

	memset(buf, 0, sizeof(buf));
	return (&k);
}
