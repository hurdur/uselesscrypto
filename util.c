/* Michael Bombardieri places this file in the public domain. */

#include <err.h>
#include <stdlib.h>
#include <string.h>

#ifdef __OpenBSD__
#define HAS_STRTONUM 1
#define HAS_STRLCAT 1
#endif

void
local_strlcat(char *dst, const char *src, size_t bufsize)
{
#ifdef HAS_STRLCAT
	(void) strlcat(dst, src, bufsize);
#else
	(void) strcat(dst, src);
#endif
}

#define NAME "local_strtonum"
int
local_strtonum(const char *s, int min, int max)
{
#ifdef HAS_STRTONUM
	const char *strtonum_err;
#endif
	int rv;

	if (s == NULL)
		errx(1, "%s: passed a null pointer", NAME);
#if HAS_STRTONUM
	rv = (int) strtonum(s, min, max, &strtonum_err);
	if (strtonum_err != NULL)
		errx(1, "%s: invalid input: %s", NAME, strtonum_err);
#else
	rv = atoi(s);
	if (rv < min || rv > max)
		errx(1, "%s: out of range: %s", NAME, s);
#endif
	return (rv);
}
#undef NAME
