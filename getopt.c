/* Michael Bombardieri places this file in the public domain. */

#include <err.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "crypt_tool.h"

#define OPTNAME_MAX 8

enum opt {
	OPT_ENCRYPT = 0,
	OPT_DECRYPT = 1,
	OPT_KEYFILE = 2,
	OPT_INFILE  = 3,
	OPT_OUTFILE = 4,
	OPT_NOSALT  = 5,
};

struct opt_kv {
	char *keyname;
	enum opt value;
};

static void usage(void);

#define NAME "local_getopt"
struct opt_final *
local_getopt(int argc, char *argv[])
{
	static struct opt_kv options[] = {
		{"e", OPT_ENCRYPT},
		{"d", OPT_DECRYPT},
		{"kfile", OPT_KEYFILE},
		{"in", OPT_INFILE},
		{"out", OPT_OUTFILE},
		{"nosalt", OPT_NOSALT},
		{NULL, -1}
	};
	static struct opt_final rv = {
		-1, 1, NULL, NULL, NULL
	};
	static char new_name[32];
	char *opt_k;
	int opt_v;
	int i;
	int j;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] != '-') {
			warnx("%s: dangling option: '%s'", NAME, argv[i]);
			usage();
		}
		opt_v = -1;
		opt_k = &argv[i][1];
		for (j = 0; options[j].keyname != NULL; j++) {
			if (strncmp(opt_k, options[j].keyname, OPTNAME_MAX) == 0) {
				opt_v = options[j].value;
				break;
			}
		}
		switch (opt_v) {
		case OPT_ENCRYPT:
			if (rv.encrypt == 0) {
				warnx("%s: -e and -d may not be combined", NAME);
				usage();
			}
			rv.encrypt = 1;
			break;
		case OPT_DECRYPT:
			if (rv.encrypt == 1) {
				warnx("%s: -e and -d may not be combined", NAME);
				usage();
			}
			rv.encrypt = 0;
			break;
		case OPT_KEYFILE:
			i++;
			rv.keyfile = argv[i];
			break;
		case OPT_INFILE:
			i++;
			rv.infile = argv[i];
			break;
		case OPT_OUTFILE:
			i++;
			rv.outfile = argv[i];
			break;
		case OPT_NOSALT:
			rv.add_salt = 0;
			break;
		default:
			warnx("%s: unexpected option: %s", NAME, argv[i]);
			usage();
		}
	}
	if (rv.encrypt == -1) {
		warnx("%s: neither -e nor -d were specified", NAME);
		usage();
	}
	if (rv.keyfile == NULL) {
		warnx("%s: -kfile not specified", NAME);
		usage();
	}
	if (rv.infile == NULL) {
		warnx("%s: -in not specified", NAME);
		usage();
	}
	if (rv.outfile == NULL) {
		memset(new_name, 0, sizeof(new_name));
		local_strlcat(new_name, basename(rv.infile), sizeof(new_name));
		local_strlcat(new_name, ".enc", sizeof(new_name));
		if (new_name[31] != '\0')
			errx(1, "%s: overrun naming new file", NAME);
		rv.outfile = new_name;
	}
	return &rv;
}
#undef NAME

static void
usage(void)
{
	extern char *__progname;

	printf("usage: %s <-e|-d> -kfile <FILE> -in <FILE> -out <FILE>\n",
		__progname);
	exit(1);
}
