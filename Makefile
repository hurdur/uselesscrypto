CFLAGS = -Wall -O2
BIN = /usr/local/bin

all: crypt-keygen crypt-mbc1 crypt-mbc2 crypt-mbc3 \
	crypt-mbc4 crypt-mbc5 crypt-mbc6 crypt-mbc7 crypt-mbc8 \
	crypt-mbc9 crypt-mbc10

crypt-mbc1: mbc1.a shared.a crypt-mbc1.o getopt.o readkey.o
	$(CC) $(CFLAGS) -o crypt-mbc1 crypt-mbc1.o getopt.o readkey.o shared.a mbc1.a

crypt-mbc2: mbc2.a shared.a crypt-mbc2.o getopt.o readkey.o
	$(CC) $(CFLAGS) -o crypt-mbc2 crypt-mbc2.o getopt.o readkey.o shared.a mbc2.a

crypt-mbc3: mbc3.a shared.a crypt-mbc3.o getopt.o readkey.o
	$(CC) $(CFLAGS) -o crypt-mbc3 crypt-mbc3.o getopt.o readkey.o shared.a mbc3.a

crypt-mbc4: mbc4.a shared.a crypt-mbc4.o getopt.o readkey.o
	$(CC) $(CFLAGS) -o crypt-mbc4 crypt-mbc4.o getopt.o readkey.o shared.a mbc4.a

crypt-mbc5: mbc5.a shared.a crypt-mbc5.o getopt.o readkey.o
	$(CC) $(CFLAGS) -o crypt-mbc5 crypt-mbc5.o getopt.o readkey.o shared.a mbc5.a

crypt-mbc6: mbc6.a shared.a crypt-mbc6.o getopt.o readkey.o
	$(CC) $(CFLAGS) -o crypt-mbc6 crypt-mbc6.o getopt.o readkey.o shared.a mbc6.a

crypt-mbc7: mbc7.a shared.a crypt-mbc7.o getopt.o readkey.o
	$(CC) $(CFLAGS) -o crypt-mbc7 crypt-mbc7.o getopt.o readkey.o shared.a mbc7.a

crypt-mbc8: mbc8.a shared.a crypt-mbc8.o getopt.o readkey.o
	$(CC) $(CFLAGS) -o crypt-mbc8 crypt-mbc8.o getopt.o readkey.o shared.a mbc8.a

crypt-mbc9: mbc9.a shared.a crypt-mbc9.o getopt.o readkey.o
	$(CC) $(CFLAGS) -o crypt-mbc9 crypt-mbc9.o getopt.o readkey.o shared.a mbc9.a

crypt-mbc10: mbc10.a shared.a crypt-mbc10.o getopt.o readkey.o
	$(CC) $(CFLAGS) -o crypt-mbc10 crypt-mbc10.o getopt.o readkey.o shared.a mbc10.a

crypt-mbc1.o:
	$(CC) $(CFLAGS) -Dmbc1 -c -o crypt-mbc1.o main.c

crypt-mbc2.o:
	$(CC) $(CFLAGS) -Dmbc2 -c -o crypt-mbc2.o main.c

crypt-mbc3.o:
	$(CC) $(CFLAGS) -Dmbc3 -c -o crypt-mbc3.o main.c

crypt-mbc4.o:
	$(CC) $(CFLAGS) -Dmbc4 -c -o crypt-mbc4.o main.c

crypt-mbc5.o:
	$(CC) $(CFLAGS) -Dmbc5 -c -o crypt-mbc5.o main.c

crypt-mbc6.o:
	$(CC) $(CFLAGS) -Dmbc6 -c -o crypt-mbc6.o main.c

crypt-mbc7.o:
	$(CC) $(CFLAGS) -Dmbc7 -c -o crypt-mbc7.o main.c

crypt-mbc8.o:
	$(CC) $(CFLAGS) -Dmbc8 -c -o crypt-mbc8.o main.c

crypt-mbc9.o:
	$(CC) $(CFLAGS) -Dmbc9 -c -o crypt-mbc9.o main.c

crypt-mbc10.o:
	$(CC) $(CFLAGS) -Dmbc10 -c -o crypt-mbc10.o main.c

readkey.o:
	$(CC) $(CFLAGS) -c -o readkey.o readkey.c

getopt.o:
	$(CC) $(CFLAGS) -c -o getopt.o getopt.c

crypt-keygen: shared.a keygen.o
	$(CC) $(CFLAGS) -o crypt-keygen keygen.o shared.a

keygen.o:
	$(CC) $(CFLAGS) -c -o keygen.o keygen.c

shared.a: util.o random.o
	$(AR) rc shared.a util.o random.o

mbc1.a: mbc1_cfb.o mbc1_key.o mbc1_alg.o
	$(AR) rc mbc1.a mbc1_cfb.o mbc1_key.o mbc1_alg.o

mbc1_cfb.o:
	$(CC) $(CFLAGS) -Dmbc1 -c -o mbc1_cfb.o cfb.c

mbc1_key.o:
	$(CC) $(CFLAGS) -Dmbc1 -c -o mbc1_key.o btkey.c

mbc1_alg.o:
	$(CC) $(CFLAGS) -Dmbc1 -c -o mbc1_alg.o mbc1.c

mbc2.a: mbc2_cfb.o mbc2_key.o mbc2_alg.o
	$(AR) rc mbc2.a mbc2_cfb.o mbc2_key.o mbc2_alg.o

mbc2_cfb.o:
	$(CC) $(CFLAGS) -Dmbc2 -c -o mbc2_cfb.o cfb.c

mbc2_key.o:
	$(CC) $(CFLAGS) -Dmbc2 -c -o mbc2_key.o btkey.c

mbc2_alg.o:
	$(CC) $(CFLAGS) -Dmbc2 -c -o mbc2_alg.o mbc2.c

mbc3.a: mbc3_cfb.o mbc3_key.o mbc3_alg.o
	$(AR) rc mbc3.a mbc3_cfb.o mbc3_key.o mbc3_alg.o

mbc3_cfb.o:
	$(CC) $(CFLAGS) -Dmbc3 -c -o mbc3_cfb.o cfb.c

mbc3_key.o:
	$(CC) $(CFLAGS) -Dmbc3 -c -o mbc3_key.o btkey.c

mbc3_alg.o:
	$(CC) $(CFLAGS) -Dmbc3 -c -o mbc3_alg.o mbc3.c

mbc4.a: mbc4_cfb.o mbc4_key.o mbc4_alg.o
	$(AR) rc mbc4.a mbc4_cfb.o mbc4_key.o mbc4_alg.o

mbc4_cfb.o:
	$(CC) $(CFLAGS) -Dmbc4 -c -o mbc4_cfb.o cfb.c

mbc4_key.o:
	$(CC) $(CFLAGS) -Dmbc4 -c -o mbc4_key.o btkey.c

mbc4_alg.o:
	$(CC) $(CFLAGS) -Dmbc4 -c -o mbc4_alg.o mbc4.c

mbc5.a: mbc5_cfb.o mbc5_key.o mbc5_alg.o
	$(AR) rc mbc5.a mbc5_cfb.o mbc5_key.o mbc5_alg.o

mbc5_cfb.o:
	$(CC) $(CFLAGS) -Dmbc5 -c -o mbc5_cfb.o cfb.c

mbc5_key.o:
	$(CC) $(CFLAGS) -Dmbc5 -c -o mbc5_key.o btkey.c

mbc5_alg.o:
	$(CC) $(CFLAGS) -Dmbc5 -c -o mbc5_alg.o mbc5.c

mbc6.a: mbc6_cfb.o mbc6_key.o mbc6_alg.o
	$(AR) rc mbc6.a mbc6_cfb.o mbc6_key.o mbc6_alg.o

mbc6_cfb.o:
	$(CC) $(CFLAGS) -Dmbc6 -c -o mbc6_cfb.o cfb.c

mbc6_key.o:
	$(CC) $(CFLAGS) -Dmbc6 -c -o mbc6_key.o btkey.c

mbc6_alg.o:
	$(CC) $(CFLAGS) -Dmbc6 -c -o mbc6_alg.o mbc6.c

mbc7.a: mbc7_cfb.o mbc7_key.o mbc7_alg.o
	$(AR) rc mbc7.a mbc7_cfb.o mbc7_key.o mbc7_alg.o

mbc7_cfb.o:
	$(CC) $(CFLAGS) -Dmbc7 -c -o mbc7_cfb.o cfb.c

mbc7_key.o:
	$(CC) $(CFLAGS) -Dmbc7 -c -o mbc7_key.o btkey.c

mbc7_alg.o:
	$(CC) $(CFLAGS) -Dmbc7 -c -o mbc7_alg.o mbc7.c

mbc8.a: mbc8_cfb.o mbc8_key.o mbc8_alg.o
	$(AR) rc mbc8.a mbc8_cfb.o mbc8_key.o mbc8_alg.o

mbc8_cfb.o:
	$(CC) $(CFLAGS) -Dmbc8 -c -o mbc8_cfb.o cfb.c

mbc8_key.o:
	$(CC) $(CFLAGS) -Dmbc8 -c -o mbc8_key.o btkey.c

mbc8_alg.o:
	$(CC) $(CFLAGS) -Dmbc8 -c -o mbc8_alg.o mbc8.c

mbc9.a: mbc9_cfb.o mbc9_key.o mbc9_alg.o
	$(AR) rc mbc9.a mbc9_cfb.o mbc9_key.o mbc9_alg.o

mbc9_cfb.o:
	$(CC) $(CFLAGS) -Dmbc9 -c -o mbc9_cfb.o cfb.c

mbc9_key.o:
	$(CC) $(CFLAGS) -Dmbc9 -c -o mbc9_key.o btkey.c

mbc9_alg.o:
	$(CC) $(CFLAGS) -Dmbc9 -c -o mbc9_alg.o mbc9.c

mbc10.a: mbc10_cfb.o mbc10_key.o mbc10_alg.o
	$(AR) rc mbc10.a mbc10_cfb.o mbc10_key.o mbc10_alg.o

mbc10_cfb.o:
	$(CC) $(CFLAGS) -Dmbc10 -c -o mbc10_cfb.o cfb.c

mbc10_key.o:
	$(CC) $(CFLAGS) -Dmbc10 -c -o mbc10_key.o btkey.c

mbc10_alg.o:
	$(CC) $(CFLAGS) -Dmbc10 -c -o mbc10_alg.o mbc10.c

clean:
	rm -f *.a *.o crypt-*

install:
	install crypt-keygen $(BIN)
	install crypt-mbc1 $(BIN)
	install crypt-mbc2 $(BIN)
	install crypt-mbc3 $(BIN)
	install crypt-mbc4 $(BIN)
	install crypt-mbc5 $(BIN)
	install crypt-mbc6 $(BIN)
	install crypt-mbc7 $(BIN)
	install crypt-mbc8 $(BIN)
	install crypt-mbc9 $(BIN)
	install crypt-mbc10 $(BIN)
