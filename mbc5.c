/* Michael Bombardieri places this file in the public domain. */

#include <sys/types.h>

#include <string.h>

#include "crypt_lib.h"

static u_int32_t m_sbox[] = {
	0xB3E64F8E, 0xD8C23AD7, 0x5F346C91, 0x45076A88, 
	0xFD49D68D, 0xFA3322BF, 0xB6A22943, 0x8C910C6C, 
	0x41C03B63, 0xBD6946BB, 0x72B96099, 0x9452C5E1, 
	0x606A3019, 0xD01C1F68, 0x86F32690, 0x4FC01925, 
	0xC1553785, 0xFC05F93F, 0x1627101D, 0x6CE2D990, 
	0x634F1733, 0x75A18615, 0x99FD1BC1, 0x7557047E, 
	0x4A981193, 0xFE33DA17, 0x6C952E0D, 0x4F261333, 
	0xF0AE1B62, 0x6681E8D6, 0x3827E181, 0xC0904D9A, 
	0x83243498, 0x85A1C5F1, 0x14C80CAC, 0x14AF5F54, 
	0x8866BC18, 0xB840C3FA, 0x7866FA3D, 0xF825E8FB, 
	0x2A69D6F1, 0xEB773E1C, 0x0C973646, 0x040E2995, 
	0xBEA20A20, 0x5028C385, 0x45D41151, 0x0402F8FB, 
	0x09F20F30, 0xD123E475, 0xFBD9B47F, 0xC3B3ED5E, 
	0xEF12421E, 0xBE105D07, 0x06E987D2, 0x032ABD57, 
	0xC59E75DB, 0x1D7F1C3D, 0x446B6EA9, 0x4512AB87, 
	0x6BD9222B, 0x3DFE8C60, 0xFC8314E3, 0xF641D9B6, 
	0xBA909B41, 0x60616D46, 0x8388D77E, 0xC5D87CCF, 
	0x8D1E79CA, 0x02B5936C, 0x71D8575A, 0x6FCA2963, 
	0x2BBF442F, 0x2D529944, 0x9B9B4762, 0xF7EC3207, 
	0x5D70370C, 0x171A589A, 0xC43A9EA8, 0xD53547F8, 
	0x72250EBF, 0x17CDDB7E, 0xE97D7D25, 0x32F6E436, 
	0xEE2BF447, 0xDC4C16CC, 0xF030F91C, 0x7859B457, 
	0xBEA7008B, 0x605F17FE, 0xEEEADE6B, 0x4E4D27BA, 
	0x7F8C1449, 0x00C4176B, 0x0A89D3DE, 0x2A3C8C20, 
	0x7A89A89C, 0x9856D1EC, 0x41167AEB, 0x741042F4, 
	0x0B6C27C7, 0x21DC6F84, 0xCB8A4FF0, 0xB100F7D9, 
	0x4FF8A75A, 0x329E58D2, 0x800999A7, 0xF9FEBAF6, 
	0x0DF7406B, 0xADE76E0A, 0xC8ABBEF3, 0x9A21000B, 
	0x6909245C, 0xF5722FDE, 0x2D213821, 0x9D1AEF36, 
	0xB6C3C98A, 0x42E761BE, 0x9D14E574, 0x1B4E9997, 
	0x3DF5962D, 0x8260DEDC, 0xC1385736, 0x1512639D, 
	0x636805D1, 0x06F6FCE2, 0xEDACCDB2, 0x732B9FC1, 
	0x46034C5B, 0x1F22401C, 0xDC8C8240, 0x0605C14F, 
	0xEB2FB554, 0x3F58B707, 0x70DC0B85, 0xEFC242FB, 
	0xA1E7EC4B, 0x245CDA48, 0x7D759AEC, 0xB1B6F52F, 
	0x86A42FD2, 0xA127B21B, 0x8E4608A5, 0x782E1FE3, 
	0xB00371BA, 0xAAEE0947, 0x1EF3FD43, 0x7083C6B8, 
	0x9277A255, 0xFF93B178, 0xF76B60DD, 0xCD8F26F7, 
	0xF877A719, 0xE5AC672A, 0xDDDA2371, 0x655164ED, 
	0x94544027, 0x1FB41C30, 0x41A3DD16, 0x15BD0790, 
	0x09EDF0C0, 0x85CA46D3, 0x3443D7F9, 0xC40C25A8, 
	0x69BF8074, 0x5E4A3C57, 0x8BD9D6D3, 0xF0D5C666, 
	0x6FA7B261, 0x5E90B0BC, 0x7358BDAF, 0x65E113F8, 
	0xEA990B91, 0x610D5C22, 0x1310B6D5, 0x3586F983, 
	0x2904CF5B, 0x547738A1, 0x86C6235B, 0x7A2AB276, 
	0xA460FFC2, 0xE098D223, 0x1046715C, 0x0C6C70B9, 
	0x121E7E55, 0xCB2C189B, 0x6A31652E, 0x69FF0EFF, 
	0x73CA696F, 0x818FEE7F, 0x7F4291D5, 0x222AE6BA, 
	0x19E1566A, 0x44ABFBCD, 0xAD60256D, 0x904CEA2D, 
	0x4D8E546A, 0xA2254DFE, 0xA718B6A1, 0x2CB6D19D, 
	0x9BDB58EF, 0x37C0FB23, 0x3E17AED0, 0x1FBC4499, 
	0x50D7B010, 0x868FFC51, 0x2CF3B5F5, 0x9E218C84, 
	0x630F76AA, 0x36F419E7, 0x1D895402, 0xDE17F853, 
	0xB1A3B575, 0xF4B4799C, 0xA366C39C, 0xD1632274, 
	0x44568FF1, 0xBBA3302A, 0x0D8C2F9F, 0x795D7FEE, 
	0x8D5572E2, 0xC1286181, 0xFEE49609, 0x321FB391, 
	0x5F5D177D, 0xE8CA9BE4, 0xA685F47A, 0x7BDFF6CE, 
	0x9E69C258, 0xCEFC80AB, 0xCD65058B, 0xA88398C4, 
	0x1A9137A0, 0xEE512E7F, 0x52696C3B, 0xAEB65509, 
	0x6D78B9E6, 0x61EFB0EE, 0xCFF66D9F, 0x1EED9FF4, 
	0x714F700C, 0x827C723D, 0x0103C657, 0x6E3C01FD, 
	0x903ECAB2, 0x584B92CF, 0x37569537, 0x0BA392C3, 
	0x94BB4958, 0x96F63913, 0x760ECB71, 0xA31B0956, 
	0xC9C3E02E, 0x93DCEB89, 0x602386F9, 0xD4F5F359
};

static short m_swab[] = {
	0, 31, 3, 4, 7, 8, 11, 12, 15, 16, 19, 20, 23, 24, 27, 28,
	0, 9, 1, 22, 3, 12, 7, 13, 8, 15, 10, 23, 16, 29, 25, 24,
	0, 2, 3, 6, 7, 11, 12, 17, 15, 16, 18, 24, 25, 31, 28, 29,
	9, 10, 8, 11, 7, 12, 6, 13, 20, 24, 27, 28, 26, 29, 25, 30,
	0, 16, 3, 19, 6, 22, 8, 24, 11, 27, 14, 30, 1, 31, 9, 23,
	1, 17, 4, 20, 7, 23, 9, 25, 10, 26, 12, 28, 13, 29, 15, 31,
	0, 17, 1, 16, 21, 22, 7, 24, 8, 23, 11, 12, 14, 31, 15, 30,
	0, 3, 2, 4, 5, 8, 7, 9, 10, 13, 12, 14, 1, 16, 11, 26,
	0, 22, 1, 9, 3, 13, 7, 12, 8, 23, 10, 15, 16, 24, 25, 29,
	4, 7, 3, 16, 6, 8, 10, 27, 12, 30, 13, 28, 15, 29, 5, 20,
	0, 30, 3, 31, 7, 4, 11, 8, 15, 12, 19, 16, 23, 17, 27, 1,
	0, 1, 3, 5, 7, 10, 12, 19, 15, 20, 18, 16, 25, 24, 28, 31,
	9, 12, 8, 10, 7, 11, 6, 16, 20, 23, 27, 30, 26, 28, 25, 29,
	1, 18, 2, 17, 16, 19, 7, 22, 8, 21, 11, 27, 14, 12, 15, 3,
	0, 2, 3, 4, 5, 7, 8, 9, 10, 12, 13, 14, 1, 18, 11, 28,
	6, 7, 4, 8, 0, 19, 5, 23, 10, 24, 13, 25, 15, 26, 12, 31
};

char *
bt_cipher_name(void)
{
	static char name[] = "mbc5";
	return name;
}

int
bt_cipher_block(void)
{
	return 32;
}

int
bt_cipher_key(void)
{
	return 16;
}

int
bt_cipher_rounds(void)
{
	return 16;
}

void
bt_cipher_prepare(void *in_key, struct ci_key *key)
{
	u_int32_t in_word[4];
	int i, j, x;
	u_int8_t *in;

	/* in_key taken to be network byte order */
	memcpy(in_word, in_key, sizeof(in_word));
	for (i = 0; i < 4; i++)
		in_word[i] = ntohl(in_word[i]);
	in = (u_int8_t *) in_word;

	for (i = 0, j = 0; i < 16; i++, j += 4) {
		x = in[i];
		key->stream[j] = m_sbox[x];
		x = (x + 3) & 255;
		key->stream[j+1] = m_sbox[x];
		x = (x + 7) & 255;
		key->stream[j+2] = m_sbox[x];
		x = (x + 15) & 255;
		key->stream[j+3] = m_sbox[x];
	}
	for (i = 0; i < 64; i++)
		key->stream[i] = htonl(key->stream[i]);
	memset(in_word, 0, sizeof(in_word));
	i = j = x = 0;
}

void
bt_cipher_encode(void *in_blk, struct ci_key *key, int rounds)
{
	u_int32_t in_word[8];
	int i, j, x, y, z;
	u_int32_t *blk;
	u_int8_t t;
	u_int8_t *in;

	/* in_blk taken to be network byte order */
	memcpy(in_word, in_blk, sizeof(in_word));
	for (i = 0; i < 8; i++)
		in_word[i] = ntohl(in_word[i]);
	blk = (u_int32_t *) in_word;
	in = (u_int8_t *) in_word;

	for (i = 0; i < rounds; i++) {
		for (j = 1; j < 32; j++)
			in[j] = (u_int8_t)((in[j] + in[j-1]) & 255);

		x = (i << 4) & 255;
		for (j = 0; j < 16; j += 2) {
			y = m_swab[x+j];
			z = m_swab[x+j+1];
			t = in[y];
			in[y] = in[z];
			in[z] = t;
		}

		z = (i << 3) & 63;
		for (j = 0; j < 8; j++)
			blk[j] ^= key->stream[z+j];
	}

	for (i = 0; i < 8; i++)
		in_word[i] = htonl(in_word[i]);
	memcpy(in_blk, in_word, sizeof(in_word));

	memset(in_word, 0, sizeof(in_word));
	i = j = x = y = z = t = 0;
}

#ifdef ECB
void
bt_cipher_decode(void *in_blk, struct ci_key *key, int rounds)
{
	u_int32_t in_word[8];
	int i, j, x, y, z;
	u_int32_t *blk;
	u_int8_t t;
	u_int8_t *in;

	/* in_blk taken to be network byte order */
	memcpy(in_word, in_blk, sizeof(in_word));
	for (i = 0; i < 8; i++)
		in_word[i] = ntohl(in_word[i]);
	blk = (u_int32_t *) in_word;
	in = (u_int8_t *) in_word;

	for (i = --rounds; i >= 0; i--) {
		z = (i << 3) & 63;
		for (j = 0; j < 8; j++)
			blk[j] ^= key->stream[z+j];

		x = (i << 4) & 255;
		for (j = 0; j < 16; j += 2) {
			y = m_swab[x+j];
			z = m_swab[x+j+1];
			t = in[y];
			in[y] = in[z];
			in[z] = t;
		}

		for (j = 31; j >= 1; j--)
			in[j] = (u_int8_t)((in[j] - in[j-1]) & 255);
	}

	for (i = 0; i < 8; i++)
		in_word[i] = htonl(in_word[i]);
	memcpy(in_blk, in_word, sizeof(in_word));

	memset(in_word, 0, sizeof(in_word));
	i = j = x = y = z = t = 0;
}
#endif /* ECB */
