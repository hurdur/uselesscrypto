/* Michael Bombardieri places this file in the public domain. */

#include <sys/types.h>

#include <err.h>
#include <string.h>

#include "crypt_lib.h"

#define MAX_BLOCK_SIZE 64

void
bt_cfb(u_int32_t *data, int len, struct ci_key *key, u_int32_t *iv,
	int encrypt, int rounds)
{
	u_int32_t fbk[MAX_BLOCK_SIZE/4], tmp[MAX_BLOCK_SIZE/4];
	int block_bytes, block_words, limit, remainder, i, j;
	u_int32_t *np = NULL;

	if (data == NULL || key == NULL || iv == NULL)
		errx(1, "bt_cfb: data/key/IV passed as null");

	block_bytes = bt_cipher_block();
	block_words = block_bytes / 4;
	limit = len / block_words;
	if (!limit)
		errx(1, "Not enough input data to form one block");
	remainder = len % block_words;

	memset(fbk, 0, sizeof(fbk));
	memmove(fbk, iv, block_bytes);

	for (i = 0; i < limit; i++) {
		bt_cipher_encode(fbk, key, rounds);

		if (!encrypt)
			memmove(tmp, data, block_bytes);

		for (j = 0; j < block_words; j++)
			data[j] ^= fbk[j];

		if (encrypt)
			memmove(fbk, data, block_bytes);
		else
			memmove(fbk, tmp, block_bytes);

		data += block_words;
	}

	if (encrypt)
		np = data - block_words;
	else
		np = fbk;

	memmove(tmp, np, block_bytes);
	bt_cipher_encode(tmp, key, rounds);

	for (i = 0; i < remainder; i++)
		data[i] ^= tmp[i];
}
