/* Michael Bombardieri places this file in the public domain. */

#include <sys/types.h>

#include <string.h>

#include "crypt_lib.h"

static u_int32_t tab0[512] = {
	0xe7e8b55b, 0x3859e547, 0x0671902c, 0x28073c93, 0x7a5844a9, 
	0x3f2840d1, 0xfe265bee, 0x64f48201, 0xdf0accf8, 0x010d94c8, 
	0x5186f9dc, 0x0542ffcf, 0xbcb71bd9, 0x519cdf0e, 0xdb0efba0, 
	0xb887073c, 0xe2e11615, 0x0db8a6bf, 0xf9d0e799, 0x485bdd44, 
	0xf02d678e, 0x39fc50a6, 0xc99fb552, 0xea81ca5f, 0x57cbec74, 
	0x101483a5, 0x782e8dd2, 0xc9bff9c0, 0x99c383aa, 0xc1449979, 
	0xec4960f7, 0x44598d31, 0x358fb5e8, 0x884c99f6, 0x7634e713, 
	0xc2a7d08b, 0x24551ec8, 0x0b8f66ef, 0xf4e5e955, 0x2a6270a7, 
	0x0dbb9ea3, 0x67c8c27b, 0x117f3cd2, 0xbf99800c, 0x08c88d31, 
	0x848f0423, 0x9f90bfc3, 0xeefb9e91, 0x23396d6b, 0xadac0207, 
	0xcfcc8c02, 0x9d4aefb6, 0x7dbcb010, 0xb9d8a31d, 0xa7b46edb, 
	0x339030c8, 0x82b3b9e1, 0xf5021ec2, 0x50f26893, 0xceacaf8b, 
	0x0b7cafb9, 0xb9b1fbd4, 0x549ac6e2, 0x6d4fb94d, 0x629482a7, 
	0x0d65baee, 0x3d708a01, 0x859cb792, 0x2176ae55, 0x22f1c239, 
	0xefda6b90, 0x19df78a5, 0x2a81112a, 0x7d64ad33, 0x41dc5b5b, 
	0x2df74227, 0x76b6d871, 0x19a9cc4a, 0xeec80115, 0xf442ce12, 
	0xf7ae35d5, 0x04fa26f3, 0x56c1e57e, 0x931e2c2f, 0xaaba9e78, 
	0x8af03327, 0x92149f77, 0x1cc929dc, 0x9f030ef2, 0xfeb800a3, 
	0x37513b77, 0x83dd8aef, 0x292bc8a7, 0xd843c6d2, 0xe7e5feed, 
	0x6783913d, 0xaf25ada4, 0x8980ed75, 0xd0c0e376, 0x26c7d21a, 
	0xc13fd08e, 0x310317ce, 0x6e443fcb, 0x21c6ef33, 0xbcc80a9c, 
	0x42243433, 0xdc745109, 0x1c657c1d, 0xa1b46dd8, 0xeafb505b, 
	0x132bd065, 0xa5373bb3, 0x157276e0, 0x731cc250, 0x1d6ea8c5, 
	0x77054b7b, 0xc3f4ca4f, 0x77547461, 0x67380c29, 0xd0acf05a, 
	0x8655999a, 0x584a1be8, 0xf30f4aaa, 0x4f37002b, 0x1970f0dc, 
	0xa9d0cfbd, 0xd738fd97, 0xcaf38433, 0x107e1949, 0x17414b2c, 
	0x770b3e88, 0x1f799d10, 0x5a28b7f7, 0x2411129f, 0x1dddf539, 
	0xcb4293e4, 0x91004ba9, 0x48a9ccef, 0x4eb35adc, 0x00daa22b, 
	0x9b5a8f06, 0xaab18620, 0x27336b65, 0xb2002da3, 0x28d5cef1, 
	0x98071886, 0x19e442b5, 0xc9950bf2, 0xae961ecf, 0x99abba1d, 
	0x1285e1df, 0x974fc987, 0xe362d2fe, 0xb9165de6, 0x922a3b10, 
	0x2b15c7e3, 0x30e5685b, 0x3df0f2ba, 0x14e9106d, 0xb9927e33, 
	0x09308c39, 0xe166ea62, 0x7eaa20b8, 0xbd9bb52a, 0xde9c8247, 
	0xfb303b83, 0xe5e13bc9, 0x3a265865, 0xeefb4923, 0x84ae082d, 
	0x9114ad5a, 0x1bbb7dec, 0xec1e94ec, 0x8f511622, 0x7c2b337a, 
	0x1c2353a1, 0xb96fb48e, 0x7aeda85e, 0x68b4a478, 0x30a8a75a, 
	0x2cd32afc, 0xdcfeff85, 0x90bdd2da, 0x36cc1cac, 0x51c81d0b, 
	0xbfb200ee, 0x272b32a8, 0x3804b41d, 0x450a3a02, 0x48bdec99, 
	0x0148ebd7, 0x94f42a20, 0xe85785a4, 0xce6ce218, 0x7409d2cd, 
	0xbe586ec1, 0x4a8d521e, 0x0c9d123e, 0xa0f54726, 0xe682d045, 
	0x5ed858f2, 0x60e7889b, 0x9d726f52, 0x127194ac, 0xebb49479, 
	0x9b3269ed, 0x4b804c5f, 0xf8b9e7a4, 0x09ccc14d, 0x32ecd5be, 
	0xad551b79, 0x98773b2a, 0xc3b6e00b, 0xdc1c86ea, 0x8471c632, 
	0x9b1528de, 0x03a7da80, 0xa5b65b16, 0x8c2e0068, 0xbc486205, 
	0x75a9e750, 0x7f51234e, 0x91bbae0c, 0x0ff45223, 0xccb0587b, 
	0xda1f46a8, 0x6ffad04a, 0x17a15cbb, 0x29dfe4f2, 0xb95cbc36, 
	0x1011fc84, 0x61c1f825, 0xc64d49d6, 0xdbb63e96, 0xf6177d34, 
	0xcd7bf083, 0xaa4cc29f, 0xfddfecd5, 0x389d6bad, 0x4d47d721, 
	0xe6b3d1de, 0x5cda8305, 0x1a02341d, 0xc060e02d, 0x150f05b2, 
	0x51dee35f, 0x76cdf879, 0x5c182ea6, 0xbda897c1, 0x01965e3b, 
	0x5f05fb7b, 0x38905adc, 0x3cc4233c, 0x6540b2fd, 0x7685741b, 
	0x33f66fec, 0xf14d35a2, 0xf3a59082, 0xe538a820, 0xe05c2f9e, 
	0x6aaad6a2, 0xcbd821d3, 0x456d68c5, 0x34a418ca, 0xe5622371, 
	0x3481776d, 0x2f1b6e92, 0xf1645816, 0xb78103ab, 0x1ee56940, 
	0xfda0c1bc, 0x7d13578a, 0xf2b265ed, 0x6d9b005b, 0x9b9d9a19, 
	0xd7fa7f8a, 0x85746374, 0x1ae04e34, 0x797cba77, 0x96bb75dd, 
	0x892a9599, 0xcf25b625, 0x9aa21b56, 0x3c7fb46a, 0x6f788216, 
	0xdc05be67, 0x765ae6b1, 0xd3a155ec, 0x9e8c71a2, 0xadbc1ba9, 
	0x60562b1f, 0xc139cf33, 0x32269976, 0x62a290ac, 0x4bf82cae, 
	0x572bdf41, 0xd4de8c6e, 0xc64d79df, 0x4178f5b7, 0x7c52dd81, 
	0x3ab0d0c2, 0x4133877c, 0xce4de583, 0xecddb3fc, 0x17cb720f, 
	0x4bf19c45, 0xbfc729ed, 0x7ae02cb8, 0xf49bd8b7, 0x634c2775, 
	0xc2c8ef20, 0x6b9799a2, 0x21ae6d1e, 0x3a908d33, 0x0215a9ac, 
	0x41f0c7da, 0x81f1cfef, 0x91381e5d, 0xfa6c22a0, 0xf8a9a099, 
	0x5c22fa4d, 0x844bc23b, 0x480e71d7, 0x1df47280, 0xb5d4362f, 
	0x5192b677, 0x69cf8798, 0xaafdba9a, 0x56941d43, 0x4ea1a40e, 
	0x4f6b2bcc, 0xfdf0b3ed, 0x889bc614, 0x8e875a8c, 0x78cf2fae, 
	0x561d2fc7, 0x47e564e8, 0x116d4234, 0xce133895, 0x232d39ec, 
	0x80c6054c, 0xe589f7b5, 0xcfd2f340, 0x05ab4744, 0x69a33abd, 
	0xb34d4314, 0x16a68eb0, 0xc339a172, 0x8d9aa47e, 0x567b95fe, 
	0xaf32102b, 0xf315ce4a, 0x1713fe0f, 0x8fc90c14, 0x640dcafc, 
	0xbc725318, 0xdac2c1f4, 0x7b9dfea4, 0xb252af0a, 0xa6218aa9, 
	0x9dc77eae, 0x20b86b36, 0xd37b5e1b, 0xe1ee4242, 0x19c69d26, 
	0xe895d528, 0x8cf8b118, 0xdafa2f63, 0xed89cdf4, 0xd88718a6, 
	0x1a3597d2, 0x6ef1d2a8, 0x95807ede, 0x6e989f48, 0x20b1f509, 
	0x53b4c707, 0x3c773953, 0x544b1d99, 0xc50e3cca, 0xee18e901, 
	0x71549ade, 0xdb202d59, 0x374ec76e, 0x9247a53d, 0x4218561d, 
	0x02fc9bfa, 0x39461d1f, 0x695716ed, 0x0a44e159, 0x21e044b5, 
	0x667a8042, 0xa1df765b, 0x8defa7c4, 0xf07c9293, 0xf788d3eb, 
	0x0924fcec, 0xccf075f7, 0xb91f2bbb, 0xea8898ae, 0x19ca50c8, 
	0xa0c36626, 0xf6d5a779, 0x58457923, 0x268cdf5e, 0x3003597c, 
	0x463ac9c0, 0xe13908e8, 0x3ad87e3c, 0x0c486fd0, 0xd467163d, 
	0x61d59c90, 0x4d1f28f6, 0x434a4afe, 0xb033a160, 0xaf581bb3, 
	0xddb00205, 0x53f555f3, 0x73cddc52, 0xf33443a4, 0xed582011, 
	0x566cf5b2, 0x3058657e, 0xc1da7473, 0x882b237d, 0xfb4026f3, 
	0xcd991b06, 0xa0803188, 0x31b3d22c, 0x2430c195, 0x586f5c43, 
	0x8e074ba9, 0xa4eaf7f6, 0x9bea9610, 0x822efb5b, 0xab917b32, 
	0x12e5f1dd, 0x787187b9, 0x4be99fc2, 0xd3f5f894, 0xcbe7bb44, 
	0xd82d676f, 0xfbe92a5a, 0x71dc0d8f, 0x6a321c22, 0x767ccdf3, 
	0x3b62bfae, 0xd92e3300, 0xbb1445a4, 0x9e5b320a, 0x3310010b, 
	0x148a287c, 0x1c098da6, 0x748d6f5f, 0xb4e0b384, 0x7c03fc41, 
	0xbae53edf, 0xf8c407a3, 0x091680a6, 0x54a97c06, 0xff4be829, 
	0x35499b00, 0x93bcd43f, 0x8e8a52cf, 0x41efa8ef, 0x86e84029, 
	0x1a06ba34, 0x83f4c4a7, 0x14ff5014, 0x0ab1486f, 0x31ad71f8, 
	0x110a7d4c, 0xb350f50e, 0xebb6b596, 0x6baeea6e, 0x7998e1fe, 
	0xa9dd1546, 0x95e47e77, 0xbc6f2265, 0x669ac679, 0x4ec3afaf, 
	0x7c019b2c, 0x6eb77621, 0x4368f42a, 0xa9bccf28, 0xb354453b, 
	0xdf0f0f6b, 0xe40284d8, 0x60d93ebb, 0x6d3451cf, 0x7ba2f1a3, 
	0xd23ae6d8, 0x1950ba69, 0x09402404, 0x8dfe1853, 0x1533ac3a, 
	0x1e5eeb57, 0xcbaf6b93, 0x48eef81f, 0x60a9a282, 0x75557c13, 
	0xac440895, 0x7463921a, 0xcdc719cd, 0x9eb12800, 0x2c2bc033, 
	0x052bb9ab, 0x2411ac62, 0xd02a7299, 0x9094fb71, 0xb01acdde, 
	0x45345e8b, 0x30917aa7
};

static int mixlist[6][2] = {
	{0, 1},
	{2, 3},
	{0, 2},
	{1, 3},
	{0, 3},
	{1, 2}
};

char *
bt_cipher_name(void)
{
	static char *name = "mbc9";
	return (name);
}

int
bt_cipher_block(void)
{
	return (16);
}

int
bt_cipher_key(void)
{
	return (64);
}

int
bt_cipher_rounds(void)
{
	return (32);
}

/* non-linear functions to combine with array index in key schedule */
static int
f512(int i)
{
	i++;
	return ((2 * i * i + i + 303) & 0x1ff);
}

static int
g512(int i)
{
	i = (50000 - i) % 99 + 1;
	return (((i * i * i) + 21) & 0x1ff);
}

static int
h512(int i)
{
	i = (i + 1) * 2;
	return (((511 * i) * i) & 0x1ff);
}

static int
i512(int i)
{
	i = (255 + i) * 3;
	return ((i * i + 77) & 0x1ff);
}

/* Add individual bytes in a word */
static u_int8_t
addword(u_int32_t w)
{
	u_int8_t a, sum;

	sum = 0;
	a = w & 0xff;
	sum = (sum + a) & 0xff;
	a = (w >> 8) & 0xff;
	sum = (sum + a) & 0xff;
	a = (w >> 16 ) & 0xff;
	sum = (sum + a) & 0xff;
	a = (w >> 24) & 0xff;
	sum = (sum + a) & 0xff;
	return sum;
}

/* take the round number and round key, generate a key */
static void
roundkey_init(struct rndkey *rkey, int i, struct ci_key *k)
{
	u_int32_t x;
	int j, p, q;

	x = k->seed ^ k->x[0];
	for (j = 0; j < 8; j++, i++)
		x = (x + k->x[i % 80]) & 0xffffffff;
	q = addword(k->seed ^ x);
	p = addword(x);
	rkey->a = T(f512(p));
	rkey->b = T(g512(p));
	rkey->c = T(h512(q));
	rkey->d = T(i512(q));
	rkey->d ^= k->seed;
	rkey->c ^= rkey->d;
	rkey->b ^= rkey->c;
	rkey->a ^= rkey->b;
	rkey->a ^= k->x[i % 80];
	rkey->b ^= k->x[(i * 2) % 80];
	rkey->c ^= k->x[(i * 3) % 80];
	rkey->d ^= k->x[(i * 5) % 80];
	rkey->a ^= k->dust;
	rkey->b ^= k->dust;
	rkey->c ^= k->dust;
	rkey->d ^= k->dust;
}

void
bt_cipher_prepare(void *in_key, struct ci_key *key)
{
	u_int32_t *in;
	u_int32_t newkey[16];
	u_int32_t a, b, c, d, seed;
	u_int8_t p, q, r, s;
	int i, j, sum, rot_bits;

	in = in_key;
	a = seed = 0;
	for (i = 0; i < 16; i++) {
		b = f512(i) * g512(i+1);
		b <<= 16;
		b += h512(i+2) * i512(i+3);
		c = (b + T(f512(i))) & 0xffffffff;
		d = c ^ in[i];
		newkey[i] = d;
		seed = (seed + d) & 0xffffffff;
	}

	newkey[15] ^= seed;
	for (i = 14; i >= 0; i--)
		newkey[i] ^= newkey[i+1];

	newkey[0] = (newkey[0] + newkey[15]) & 0xffffffff;
	for (i = 1; i < 16; i++)
		newkey[i] = (newkey[i] + newkey[i-1]) & 0xffffffff;

	for (i = 0; i < 16; i++) {
		j = (i + 1) & 0xf;
		sum = addword(in[i]) + addword(in[j])
			+ addword(newkey[i]) + addword(newkey[j]);
		rot_bits = sum & 0x1f;
		p = f512(sum);
		q = g512(sum);
		r = h512(sum);
		s = i512(sum);
		newkey[i] ^= T(p);
		newkey[i] = ROTL32(newkey[i], rot_bits);
		newkey[i] = (newkey[i] + T(q)) & 0xffffffff;
		newkey[j] = (newkey[j] + T(r)) & 0xffffffff;
		newkey[j] = ROTL32(newkey[j], 31 - rot_bits);
		newkey[j] ^= T(s);
	}

	newkey[0] = (newkey[0] + newkey[15]) & 0xffffffff;
	for (i = 1; i < 16; i++)
		newkey[i] = (newkey[i] + newkey[i-1]) & 0xffffffff;

	for (i = 15 ; i >= 0; i--) {
		j = (i + 1) & 0xf;
		sum = addword(in[i]) + addword(in[j])
			+ addword(newkey[i]) + addword(newkey[j]);
		rot_bits = sum & 0x1f;
		p = f512(sum);
		q = g512(sum);
		r = h512(sum);
		s = i512(sum);
		newkey[i] ^= T(p);
		newkey[i] = ROTL32(newkey[i], rot_bits);
		newkey[i] = (newkey[i] + T(q)) & 0xffffffff;
		newkey[j] = (newkey[j] + T(r)) & 0xffffffff;
		newkey[j] = ROTL32(newkey[j], 31 - rot_bits);
		newkey[j] ^= T(s);
	}

	for (i = 0,  j = 7; i < 80; i++, j++) {
		a = j * 909091;
		a = ROTL32(a, j & 0x1f);
		key->x[i] = (newkey[i & 0xf] + a) & 0xffffffff;
		seed ^= key->x[i];
		b = (b + seed) & 0xffffffff;
	}
	key->seed = seed;
	key->dust = b;

	memset(newkey, 0, sizeof(newkey));
	a = b = c = d = seed = p = q = r = s = i = j = sum = rot_bits = 0;
}

void
bt_cipher_encode(void *in_blk, struct ci_key *key, int rounds)
{
	u_int32_t *blk;
	struct rndkey rndkey;
	int a, b, i, j;
	u_int32_t t;

	blk = in_blk;
	for (i = 3, j = 79; i >= 0; i--, j--)
		blk[i] ^= key->x[j];

	for (i = 0; i < rounds; i++) {
		roundkey_init(&rndkey, i, key);

		for (j = 1; j < 4; j++)
			blk[j] ^= blk[j-1];
		blk[0] ^= blk[3];

		j = addword(rndkey.a) % 6;
		a = mixlist[j][0];
		b = mixlist[j][1];
		t = blk[a];
		blk[a] = blk[b];
		blk[b] = t;

		blk[0] ^= rndkey.d;
		blk[1] ^= rndkey.c;
		blk[2] ^= rndkey.b;
		blk[3] ^= rndkey.a;
	}

	memset(&rndkey, 0, sizeof(rndkey));
	a = b = i = j = t = 0;
}

#ifdef ECB
void
bt_cipher_decode(void *in_blk, struct ci_key *key, int rounds)
{
	u_int32_t *blk;
	struct rndkey rndkey;
	int a, b, i, j;
	u_int32_t t;

	blk = in_blk;
	for (i = rounds - 1; i >= 0; i--) {
		roundkey_init(&rndkey, i, key);

		blk[0] ^= rndkey.d;
		blk[1] ^= rndkey.c;
		blk[2] ^= rndkey.b;
		blk[3] ^= rndkey.a;

		j = addword(rndkey.a) % 6;
		a = mixlist[j][0];
		b = mixlist[j][1];
		t = blk[a];
		blk[a] = blk[b];
		blk[b] = t;

		blk[0] ^= blk[3];
		for (j = 3; j > 0; j--)
			blk[j] ^= blk[j-1];
	}

	for (i = 3, j = 79; i >= 0; i--, j--)
		blk[i] ^= key->x[j];

	memset(&rndkey, 0, sizeof(rndkey));
	a = b = i = j = t = 0;
}
#endif /* ECB */
