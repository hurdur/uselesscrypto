/* Michael Bombardieri places this file in the public domain. */

#include <sys/types.h>

#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#define RANDOM "/dev/random"

void *
getrandom(int bytes)
{
	void *np;
	int got_bytes;
	int fd;

	if (bytes <= 0)
		return (NULL);
	np = malloc(bytes);
	if (np == NULL)
		return (NULL);
	fd = open(RANDOM, O_RDONLY, 0644);
	if (fd == -1)
		goto fail2;
	got_bytes = (int) read(fd, np, bytes);
	if (got_bytes != bytes)
		goto fail;
	if (close(fd) == -1)
		goto fail2;
	return (np); /* caller to free() */
fail:
	(void) close(fd);
fail2:
	free(np);
	return (NULL);
}
