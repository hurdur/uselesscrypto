/* Michael Bombardieri places this file in the public domain. */

#include <sys/types.h>

#include <string.h>

#include "crypt_lib.h"

static u_int32_t key_iv[256] = {
	0xEF3F0A8F, 0x59249A94, 0x7453E351, 0x8CCA116C, 
	0x01D0DAAA, 0x19749B7F, 0xDF9A2C63, 0x33CC0FE3, 
	0xCF94A21A, 0x4D0952F9, 0x43E890AF, 0xBCED8219, 
	0x425DD74A, 0xFF0F08DD, 0x3CB7C384, 0x996AC009, 
	0xEA647412, 0x34DD24A1, 0x513C865A, 0x0B0E6DFE, 
	0xC3AA77C1, 0xCDEDE0FF, 0x0A631B58, 0x3D68AA55, 
	0xD09DED78, 0x184A50E6, 0x25A877C5, 0x3A108470, 
	0xC0A4A66D, 0xD8795504, 0xCA43D9AA, 0xFC8F8E8D, 
	0xBB2B2377, 0x113FF524, 0x86A25301, 0x10A7CF4E, 
	0xC76DF4AF, 0xBCEC493F, 0x1DB2BDF0, 0xE438F155, 
	0xD0E50E05, 0xD7700403, 0x5BF8BBF6, 0xBABDAA8D, 
	0x54895BBF, 0xCA7F7A80, 0x06D25BA9, 0xE67B7DA4, 
	0xE6CAB11E, 0x8E48DF28, 0x548430D8, 0x882C065E, 
	0x651A1251, 0x3C466FC2, 0x92D7AEDD, 0x6CA7EC0C, 
	0xE12B3A1D, 0x076AB991, 0xC5A63F34, 0xC73D92E0, 
	0xAE6A65C1, 0xB72BCA22, 0x412F8DE6, 0x921077D8, 
	0x25B31C25, 0x23A58377, 0x344E5CC7, 0x9C80C191, 
	0xB3308536, 0xC02647C5, 0xCE798103, 0x4028D382, 
	0x1FD03396, 0x2E9B5C8E, 0xF1EAC961, 0x6E01F883, 
	0x5BB4CF8F, 0x398E8F50, 0xB199274D, 0x9C0AABEB, 
	0x34FCC790, 0xFA1C48A6, 0x5BDED374, 0x5EE8367A, 
	0x8D271374, 0x46C9C32A, 0x5585A003, 0xFFE13BD4, 
	0x639284F5, 0x28B44F4B, 0x321706CE, 0x464A5695, 
	0xD2ED34BD, 0x9E810BCE, 0x79DE78F7, 0x77841E49, 
	0xF72CFE87, 0xA0C038D7, 0xD698AB92, 0xE2BC61EA, 
	0x27EA76FE, 0x41C22007, 0xF3EB5E14, 0x8B9C5BC3, 
	0x22F5BC95, 0xBB660B3F, 0xEB3AE4AA, 0x1A93BDBE, 
	0x39283719, 0x3BD974F5, 0x1B674806, 0x12DBD744, 
	0x35FDA4B0, 0x845571C7, 0xEE3E5B8C, 0xFF9C81BE, 
	0xAB783EE9, 0x05C9FA5F, 0x42A97E68, 0x36905884, 
	0xEB6A0354, 0x0038B98A, 0x34A5E1FE, 0x6D650B04, 
	0xDF4A0A0B, 0x2DCC3095, 0x4BE0F63F, 0xA1BE5FD2, 
	0xAD5BCB36, 0xCA3CC703, 0x032DB472, 0x7528D7B4, 
	0xEFFBDAE9, 0x441CAD0A, 0xE4F7367D, 0xA3681FBB, 
	0xCE1058BF, 0x43400DA3, 0xDEE3B3BD, 0x9B244453, 
	0xCA24DF4E, 0xE4E3EF43, 0x7DB2C0BD, 0x246A3733, 
	0xACA1082D, 0xF2B76076, 0x960409A3, 0x1AC5C607, 
	0xEB01185A, 0x65147BD5, 0x63AC96DB, 0x308739ED, 
	0xCCF32B1F, 0x2AB030EE, 0x5E3B4C4E, 0xFE176289, 
	0x8D224C20, 0x0FA27A24, 0xD7C685CB, 0x938E131A, 
	0xF4980AB3, 0xACE49655, 0x73CD23AD, 0x2629D61E, 
	0x2E4FEB90, 0xC8513A47, 0xD9E1AC6A, 0x2A4E2259, 
	0x00C161B6, 0x764CB368, 0x3C19C130, 0x066A5D78, 
	0x2EEBD9CE, 0xF846147D, 0x43686D02, 0x33E7C3BD, 
	0xC4856A1B, 0x8410E39D, 0x3A9B8111, 0x4305F57A, 
	0x132CD267, 0xAE9FAE7C, 0x5ECDC5FD, 0x1E882F55, 
	0xD2373C81, 0xB52AED47, 0xD9C6CEC2, 0x097F9478, 
	0xC9F338B6, 0x6146D5FE, 0x2A9B1857, 0x5190A323, 
	0x54749BEB, 0xF6B3A718, 0x5D40818E, 0x2DE73DFA, 
	0xDDED91C0, 0x5E08A1AE, 0xBB5EB5F4, 0x2F7A13D2, 
	0x1A8578D1, 0xFACA44B1, 0xE90BE4FE, 0x64439A47, 
	0x8E890FF5, 0x5ADD05B9, 0x3CE8B801, 0xFF442744, 
	0x1F6B421A, 0xC9694FAA, 0x5D47AEAF, 0xA1BF49E1, 
	0xBC0F34C2, 0xFCFD50DA, 0x7CFE7A44, 0x1CF2F115, 
	0x992FAB99, 0xA5D541F3, 0x760603F7, 0x79B6C98D, 
	0x3F5087B4, 0x3FB506E7, 0xAD95D9B7, 0x02CDD863, 
	0xB4DAC5D1, 0x58763047, 0x7208580B, 0xAE6D639F, 
	0xEEAB5B82, 0x4AD8FBA0, 0x01C67417, 0x24048E37, 
	0x2ADAA4E3, 0xCD0F3975, 0x5396365F, 0x89C1A2F5, 
	0x26F24EB4, 0x4DBD1360, 0x4B318393, 0xEECFFD33, 
	0x9BD3F795, 0xDE1A7F7E, 0x205C722C, 0xD68BA725, 
	0x614F258B, 0x0ED531E4, 0x79D1EB1C, 0x5590E9F2, 
	0x716F6369, 0x756CFEFD, 0x8CD06514, 0x61F6CEB9, 
	0x41024F76, 0xFAD36BC1, 0xBA160913, 0xA89DBED2
};

static u_int32_t key_iv2[256] = {
	0xFE182316, 0x63B66355, 0xD5BB7EE1, 0x4FD63C3C, 
	0x5A15F240, 0xDC843423, 0xD6CAC18B, 0x2E92AF06, 
	0x1D6CC0CE, 0x19519D9F, 0xA15F7ED7, 0x8DE36D4E, 
	0x136B8723, 0x902EDEFB, 0x27561136, 0x502D5635, 
	0x770DA740, 0x8346C517, 0xB8E55886, 0x00B9CDDA, 
	0xAFB6417D, 0x9FF2B6B1, 0xB9E7E3C3, 0x0E7E7890, 
	0x47E8A3A2, 0x29DFC1C8, 0x706006BC, 0x4AF8A0F3, 
	0x855C4729, 0x64E39887, 0x9A63E6A0, 0x235FBD01, 
	0xE9408290, 0x1C5D1E0A, 0x38989A8A, 0x91BE0827, 
	0x09D1976A, 0x35CEB2F4, 0xF16C3008, 0xA83A97E7, 
	0x8AA32F7A, 0x7C79C44A, 0x16AD1E23, 0xF0E7A3F7, 
	0xDC4250F7, 0xC523989A, 0x7E8FC1E0, 0x9E3597E0, 
	0x9BDE9126, 0x6F1C3A92, 0xE1DF425C, 0x51E59057, 
	0xC0C74B2E, 0x5C3413AC, 0x485F89E5, 0xDD711019, 
	0xF307FC0F, 0xF8B454FD, 0xF32A5B96, 0xC9935ABA, 
	0xC69EE71C, 0xF3F5B9EA, 0xD9D2B5F4, 0x7DB70561, 
	0x2617C739, 0x2AF127E3, 0x93B9597D, 0x12BF85FA, 
	0xC6C95490, 0xD5B9E8ED, 0xA2C209F8, 0xFB35CAF9, 
	0x699A165D, 0x912D3815, 0xAA3A6329, 0xD1E1E010, 
	0x02FF1484, 0x8AFE6272, 0x47115698, 0x2523233A, 
	0x39C23B16, 0xED3A5F94, 0x66D844AA, 0x9ADAC65C, 
	0x72250DDB, 0x26A839ED, 0xD9AE9BD1, 0x87012404, 
	0x38953B77, 0xF4ED6218, 0xD6A49B78, 0x78E51896, 
	0xCE2FC156, 0xF5A80ED4, 0xCE4DA1E4, 0x9931D6C0, 
	0x902F0CDB, 0x8BEB4B09, 0xBDE9DDFA, 0x4D7E0E21, 
	0x3B05D8FC, 0xED417381, 0x9722240F, 0xD78A2DF3, 
	0x80693D21, 0x28DB9B9B, 0xC11784CF, 0x7185A11C, 
	0x2F91A468, 0xE2C570CF, 0xF99F17A6, 0x8BE664BD, 
	0x03B513E4, 0x3C7D81A1, 0x9CFD42D7, 0x9C8A21FF, 
	0x72B8B325, 0xBF7DFC61, 0x1323AC30, 0x6FD31DA8, 
	0xBBEFA8DA, 0x21427462, 0xEA54CB02, 0x95147D02, 
	0x721127D8, 0x5841D3F7, 0x0D55EDD1, 0x89F9FF65, 
	0x0A64FEFF, 0x54FE8106, 0x91E919DF, 0x8BEB4BDA, 
	0x6D5F180A, 0xEE2FBF77, 0xFECB5710, 0x51492781, 
	0x4B2AC9FB, 0x37A2C193, 0x4070BE80, 0x2C1E6390, 
	0xB0D657A0, 0xD912C81C, 0x55226BFE, 0x9AC00B92, 
	0xD5FCF2A7, 0x3E8D59C3, 0x4DF38091, 0xE534C0A5, 
	0x5ED234D5, 0x26E67C8C, 0x1FA62093, 0x91FD5470, 
	0x8D12BB10, 0x52B71F5A, 0xBEE997F0, 0x5BE40A49, 
	0x61A1653E, 0xA035D1F8, 0x1A1EC665, 0x7EAB167F, 
	0x98F132E5, 0x1528FCB1, 0x65AE9E7E, 0xED322160, 
	0xDB06C213, 0xA9AEECF8, 0x7A01FF54, 0xE9135EF6, 
	0x45529A08, 0x30797A66, 0x692228D3, 0xA6F509D7, 
	0x348F9BE7, 0x4552EC6C, 0x591B171E, 0x622EE3C0, 
	0x2C5847CE, 0xF7B1170E, 0xE93CC90E, 0x36FB6426, 
	0x1F68F81E, 0x1A1C22D4, 0xB8AEB289, 0xA89E32A8, 
	0xBFF621BB, 0x3518B587, 0x02077B3D, 0x1E2EF3B8, 
	0x4946D761, 0x8D338E19, 0xA6E59DAC, 0x151D061F, 
	0x64BA28C3, 0x03A6D790, 0x1FA64C42, 0x62C414F8, 
	0x702CC155, 0x7739E585, 0x4F8F3C91, 0x032405EA, 
	0x04A0165C, 0x6D9027EF, 0x5A612931, 0x89758515, 
	0x0D0B63DE, 0x3C3DD899, 0x587CAC6F, 0x76D44482, 
	0x2C99D06B, 0xB7CA03E7, 0x77B0F8D3, 0xA4D1A693, 
	0x5817C5DF, 0x9BECB004, 0x64DCFB64, 0x6D36975D, 
	0x9D6C34B6, 0xF22BC685, 0xC10EA527, 0xC572E015, 
	0x885FB748, 0xF21420EC, 0xA861790D, 0x8F8DC74B, 
	0x93BF7A3E, 0x5C44689F, 0xE67E133F, 0x69DEC45D, 
	0x27456A05, 0xE8C564C4, 0x3DB203A0, 0xCE3CDE6D, 
	0x98144F26, 0xC6FDF42A, 0xDF88A26D, 0xC96261C6, 
	0x274FCCB8, 0xE32BC134, 0xB51A9F37, 0xF3361986, 
	0xC3674492, 0x8ADC88D7, 0x61B0FABE, 0xF0308442, 
	0x61B71EC2, 0xE7689E65, 0x5BCF4583, 0x307AC140, 
	0xF05AD14D, 0x25ECB1D3, 0x20389186, 0x246DB337, 
	0x427BCBE9, 0x83C49585, 0xE2EC5B19, 0xEABBC071
};

char *
bt_cipher_name(void)
{
	static char name[] = "mbc3";
	return name;
}

int
bt_cipher_block(void)
{
	return 8;
}

int
bt_cipher_key(void)
{
	return 16;
}

int
bt_cipher_rounds(void)
{
	return 13;
}

void
bt_cipher_prepare(void *in_key, struct ci_key *key)
{
	u_int32_t in_word[4];
	u_int32_t rnd_key;
	u_int8_t *in;
	int i, j;

	memcpy(in_word, in_key, sizeof(in_word));
	for (i = 0; i < 4; i++)
		in_word[i] = ntohl(in_word[i]);
	in = (u_int8_t *) in_word;

	for (i = 0; i < 13; i++) {
		j = (int) ((in[i] + i) & 0xFF);
		rnd_key = (i & 1) ? key_iv[j] : key_iv2[j];
		rnd_key ^= (u_int32_t)((in[i] << 24) 
			+ (in[i+1] << 16) + (in[i+2] << 8) 
			+ in[i+3]);

		key->pm[i][0] = (u_int8_t)((rnd_key & 0xF) & 3);
		key->sb[i][0] = (u_int8_t)((rnd_key & 0xF0) >> 4);
		key->pm[i][1] = (u_int8_t)(((rnd_key & 0xF00) >> 8) & 3);
		key->sb[i][1] = (u_int8_t)((rnd_key & 0xF000) >> 12);
		key->pm[i][2] = (u_int8_t)(((rnd_key & 0xF0000) >> 16) & 3);
		key->sb[i][2] = (u_int8_t)((rnd_key & 0xF00000) >> 20);
		key->pm[i][3] = (u_int8_t)(((rnd_key & 0xF000000) >> 24) & 3); 
		key->sb[i][3] = (u_int8_t)((rnd_key & 0xF0000000) >> 28);
	}
	memset(in_word, 0, sizeof(in_word));
	rnd_key = i = j = 0;
}

void
bt_cipher_encode(void *in_blk, struct ci_key *key, int rounds)
{
	u_int8_t blk[16];
	u_int32_t in_word[2];
	int i, j, k;
	u_int8_t t;
	u_int8_t *in;

	/* in_blk taken to be network byte order */
	memcpy(in_word, in_blk, sizeof(in_word));
	in_word[0] = ntohl(in_word[0]);
	in_word[1] = ntohl(in_word[1]);
	in = (u_int8_t *) in_word;

	for (i = 0; i < 8; i++) {
		j = i << 1;
		blk[j] = (u_int8_t)(in[i] >> 4);
		blk[j + 1] = (u_int8_t)(in[i] & 15);
	}

	for (i = 0; i < rounds; i++) {
		k = i % 13;
		for (j = 0; j < 4; j++) {
			blk[ 3] = (u_int8_t)((blk[ 3] + blk[ 0]) & 15); 
			blk[ 6] = (u_int8_t)((blk[ 6] + blk[ 3]) & 15); 
			blk[ 9] = (u_int8_t)((blk[ 9] + blk[ 6]) & 15); 
			blk[12] = (u_int8_t)((blk[12] + blk[ 9]) & 15); 

			blk[15] = (u_int8_t)((blk[15] + blk[12]) & 15); 
			blk[ 2] = (u_int8_t)((blk[ 2] + blk[15]) & 15); 
			blk[ 5] = (u_int8_t)((blk[ 5] + blk[ 2]) & 15); 
			blk[ 8] = (u_int8_t)((blk[ 8] + blk[ 5]) & 15); 

			blk[11] = (u_int8_t)((blk[11] + blk[ 8]) & 15); 
			blk[14] = (u_int8_t)((blk[14] + blk[11]) & 15); 
			blk[ 1] = (u_int8_t)((blk[ 1] + blk[14]) & 15); 
			blk[ 4] = (u_int8_t)((blk[ 4] + blk[ 1]) & 15); 

			blk[ 7] = (u_int8_t)((blk[ 7] + blk[ 4]) & 15); 
			blk[10] = (u_int8_t)((blk[10] + blk[ 7]) & 15); 
			blk[13] = (u_int8_t)((blk[13] + blk[10]) & 15); 

			SWAP(&blk[0], &blk[7]);
		}

		blk[0] = ROTL4(blk[0], key->pm[k][0]);
		blk[0] = (u_int8_t)(blk[0] ^ key->sb[k][0]);
		blk[1] = (u_int8_t)(blk[1] ^ blk[0]);
		blk[2] = (u_int8_t)(blk[2] ^ blk[1]);
		blk[3] = (u_int8_t)(blk[3] ^ blk[2]);

		blk[4] = ROTL4(blk[4], key->pm[k][1]);
		blk[4] = (u_int8_t)(blk[4] ^ key->sb[k][1]);
		blk[5] = (u_int8_t)(blk[5] ^ blk[4]);
		blk[6] = (u_int8_t)(blk[6] ^ blk[5]);
		blk[7] = (u_int8_t)(blk[7] ^ blk[6]);

		blk[8] = ROTL4(blk[8], key->pm[k][2]);
		blk[8] = (u_int8_t)(blk[8] ^ key->sb[k][2]);
		blk[9] = (u_int8_t)(blk[9] ^ blk[8]);
		blk[10] = (u_int8_t)(blk[10] ^ blk[9]);
		blk[11] = (u_int8_t)(blk[11] ^ blk[10]);

		blk[12] = ROTL4(blk[12], key->pm[k][3]);
		blk[12] = (u_int8_t)(blk[12] ^ key->sb[k][3]);
		blk[13] = (u_int8_t)(blk[13] ^ blk[12]);
		blk[14] = (u_int8_t)(blk[14] ^ blk[13]);
		blk[15] = (u_int8_t)(blk[15] ^ blk[14]);
	}

	for (i = 0; i < 16; i++) {
		t = key->sb[i & 7][i & 3];
		blk[i] = (u_int8_t)(blk[i] ^ t);
	}

	for (i = 0; i < 8; i++) {
		j = i << 1;
		in[i] = (u_int8_t)((blk[j] << 4) + blk[j + 1]);
	}

	in_word[0] = htonl(in_word[0]);
	in_word[1] = htonl(in_word[1]);
	memcpy(in_blk, in_word, sizeof(in_word));

	memset(blk, 0, sizeof(blk));
	memset(in_word, 0, sizeof(in_word));
	i = j = k = t = 0;
}

#ifdef ECB
void
bt_cipher_decode(void *in_blk, struct ci_key *key, int rounds)
{
	u_int8_t blk[16];
	u_int32_t in_word[2];
	int i, j, k;
	u_int8_t t;
	u_int8_t *in;

	/* in_blk taken to be network byte order */
	memcpy(in_word, in_blk, sizeof(in_word));
	in_word[0] = ntohl(in_word[0]);
	in_word[1] = ntohl(in_word[1]);
	in = (u_int8_t *) in_word;

	for (i = 0; i < 8; i++) {
		j = i << 1;
		blk[j] = (u_int8_t)(in[i] >> 4);
		blk[j + 1] = (u_int8_t)(in[i] & 15);
	}

	for (i = 0; i < 16; i++) {
		t = key->sb[i & 7][i & 3];
		blk[i] = (u_int8_t)(blk[i] ^ t);
	}

	for (i = --rounds; i >= 0; i--) {
		k = i % 13;

		blk[3] = (u_int8_t)(blk[3] ^ blk[2]);
		blk[2] = (u_int8_t)(blk[2] ^ blk[1]);
		blk[1] = (u_int8_t)(blk[1] ^ blk[0]);
		blk[0] = (u_int8_t)(blk[0] ^ key->sb[k][0]);
		blk[0] = ROTR4(blk[0], key->pm[k][0]);

		blk[7] = (u_int8_t)(blk[7] ^ blk[6]);
		blk[6] = (u_int8_t)(blk[6] ^ blk[5]);
		blk[5] = (u_int8_t)(blk[5] ^ blk[4]);
		blk[4] = (u_int8_t)(blk[4] ^ key->sb[k][1]);
		blk[4] = ROTR4(blk[4], key->pm[k][1]);

		blk[11] = (u_int8_t)(blk[11] ^ blk[10]);
		blk[10] = (u_int8_t)(blk[10] ^ blk[9]);
		blk[9] = (u_int8_t)(blk[9] ^ blk[8]);
		blk[8] = (u_int8_t)(blk[8] ^ key->sb[k][2]);
		blk[8] = ROTR4(blk[8], key->pm[k][2]);

		blk[15] = (u_int8_t)(blk[15] ^ blk[14]);
		blk[14] = (u_int8_t)(blk[14] ^ blk[13]);
		blk[13] = (u_int8_t)(blk[13] ^ blk[12]);
		blk[12] = (u_int8_t)(blk[12] ^ key->sb[k][3]);
		blk[12] = ROTR4(blk[12], key->pm[k][3]);

		for (j = 0; j < 4; j++) {
			SWAP(&blk[0], &blk[7]);

			blk[13] = (u_int8_t)((blk[13] - blk[10]) & 15); 
			blk[10] = (u_int8_t)((blk[10] - blk[ 7]) & 15); 
			blk[ 7] = (u_int8_t)((blk[ 7] - blk[ 4]) & 15); 

			blk[ 4] = (u_int8_t)((blk[ 4] - blk[ 1]) & 15); 
			blk[ 1] = (u_int8_t)((blk[ 1] - blk[14]) & 15); 
			blk[14] = (u_int8_t)((blk[14] - blk[11]) & 15); 
			blk[11] = (u_int8_t)((blk[11] - blk[ 8]) & 15); 

			blk[ 8] = (u_int8_t)((blk[ 8] - blk[ 5]) & 15); 
			blk[ 5] = (u_int8_t)((blk[ 5] - blk[ 2]) & 15); 
			blk[ 2] = (u_int8_t)((blk[ 2] - blk[15]) & 15); 
			blk[15] = (u_int8_t)((blk[15] - blk[12]) & 15); 

			blk[12] = (u_int8_t)((blk[12] - blk[ 9]) & 15); 
			blk[ 9] = (u_int8_t)((blk[ 9] - blk[ 6]) & 15); 
			blk[ 6] = (u_int8_t)((blk[ 6] - blk[ 3]) & 15); 
			blk[ 3] = (u_int8_t)((blk[ 3] - blk[ 0]) & 15); 
		}
	}

	for (i = 0; i < 8; i++) {
		j = i << 1;
		in[i] = (u_int8_t)((blk[j] << 4) + blk[j + 1]);
	}

	in_word[0] = htonl(in_word[0]);
	in_word[1] = htonl(in_word[1]);
	memcpy(in_blk, in_word, sizeof(in_word));

	memset(blk, 0, sizeof(blk));
	memset(in_word, 0, sizeof(in_word));
	i = j = k = t = 0;
}
#endif /* ECB */
