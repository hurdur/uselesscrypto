/* Michael Bombardieri places this file in the public domain. */

#include <sys/types.h>

#include <err.h>
#include <stdio.h>

#include "crypt_lib.h"

#define NAME "bt_key"
void
bt_key(u_int32_t *data, int len, struct ci_key *key)
{
	if (data == NULL)
		errx(1, "%s: data passed as null", NAME);
	if (key == NULL)
		errx(1, "%s: key passed as null", NAME);
	if (bt_cipher_key() != len)
		errx(1, "%s: key length mismatch (expected %d bytes)",
			NAME, bt_cipher_key());
	bt_cipher_prepare(data, key);
}
#undef NAME
