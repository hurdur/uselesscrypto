/* Michael Bombardieri places this file in the public domain. */

#include <sys/types.h>

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "crypt_tool.h"

#define NAME_BUFSZ 16

struct blocksize {
	char *name;
	int len_block;
	int len_key;
};

static struct blocksize ciphers[] = {
	{"mbc1", 16, 16},
	{"mbc2", 8,  16},
	{"mbc3", 8,  16},
	{"mbc4", 12, 16},
	{"mbc5", 32, 16},
	{"mbc6", 8,  32},
	{"mbc7", 32, 32},
	{"mbc8", 64, 64},
	{"mbc9", 16, 64},
	{"mbc10", 16, 64},
	{NULL, -1, -1}
};

static int
supported_cipher(const char *algo)
{
	int i;

	for (i = 0; ciphers[i].name != NULL; i++)
		if (strncmp(algo, ciphers[i].name, NAME_BUFSZ) == 0)
			return (i);
	return (-1);
}

int
main(int argc, char *argv[])
{
	unsigned char *buf;
	char *rounds;
	char *algo;
	int idx;
	int i;

	if (argc != 3)
		errx(1, "expected two arguments: cipher_name & rounds");
	algo = argv[1];
	rounds = argv[2];
	idx = supported_cipher(algo);
	if (idx == -1)
		errx(1, "cipher unsupported: '%s'", algo);
	(void) local_strtonum(rounds, ROUNDS_MIN, ROUNDS_MAX);
	buf = getrandom(ciphers[idx].len_key + ciphers[idx].len_block);
	if (buf == NULL)
		errx(1, "random data generation failed");
	printf("%s:%s:", algo, rounds);
	for (i = 0; i < ciphers[idx].len_block; i++)
		printf("%02x", buf[i]);
	printf(":");
	for (; i < ciphers[idx].len_block + ciphers[idx].len_key; i++)
		printf("%02x", buf[i]);
	printf("\n");
	free(buf);
	return (0);
}
