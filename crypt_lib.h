/* Michael Bombardieri places this file in the public domain. */

#ifndef BT_CRYPT_LIB_H
#define BT_CRYPT_LIB_H 1

#define ADD32(a,b) (((a) + (b)) & 0xFFFFFFFF)
#define SUB32(a,b) (((a) - (b)) & 0xFFFFFFFF)
#define ADD8(a,b) (u_int8_t)(((a)+(b)) & 255) /* (a+b)%256 */
#define SUB8(a,b) (u_int8_t)(((a)-(b)) & 255) /* (a-b)%256 */
#define ROTL32(a,b) ((((a) << (b)) | ((a) >> (32 - (b)))) & 0xFFFFFFFF)
#define ROTL4(a,b) (u_int8_t)((((a) << (int)(b)) | ((a) >> (int)(4 - (b)))) & 15)
#define ROTR4(a,b) (u_int8_t)((((a) >> (int)(b)) | ((a) << (int)(4 - (b)))) & 15)
#define SWAP(a,b) t = *(a); *(a) = *(b); *(b) = t

#ifdef mbc1
#define MAGIC 0x19860719
#define P1 (p[j][0])
#define P2 (p[j][1])
#define P3 (p[j][2])
#define P4 (p[j][3])

struct ci_key {
	u_int32_t x[32];
};
#endif /* mbc1 */

#ifdef mbc2
struct ci_key {
	u_int8_t sb[16][48];
	u_int8_t pm[16][64];
};
#endif /* mbc2 */

#ifdef mbc3
struct ci_key {
	u_int8_t sb[13][4];
	u_int8_t pm[13][4];
};
#endif /* mbc3 */

#ifdef mbc4
#define SB() (key->sb[i & 15][j])
#define PM() (key->pm[i & 15][j])
#define SBOX(a) (s1[(int)a])

struct ci_key {
	u_int8_t noise[192];
	u_int8_t sb[16][12];
	u_int8_t pm[16][12];
};
#endif /* mbc4 */

#ifdef mbc5
struct ci_key {
	u_int32_t stream[64];
};
#endif /* mbc5 */

#ifdef mbc6
struct ci_key {
	u_int32_t garb[64];
};
#endif /* mbc6 */

#ifdef mbc7
struct ci_key {
	u_int8_t head[32];
	u_int8_t blob[256];
};
#endif /* mbc7 */

#ifdef mbc8
#define M0 0x20100405
#define M1 0x3ce0ee4f
#define M2 0x78fcf223
#define M3 0x47f1e42e

struct ci_key {
	u_int32_t stream[500];
};
#endif /* mbc8 */

#ifdef mbc9
#define T(x) (tab0[(x)])

struct rndkey {
	u_int32_t a;
	u_int32_t b;
	u_int32_t c;
	u_int32_t d;
};

struct ci_key {
	u_int32_t x[80];
	u_int32_t seed;
	u_int32_t dust;
};
#endif /* mbc9 */

#ifdef mbc10
struct ci_key {
	u_int32_t stream[32];
	u_int8_t swap[32];
	u_int8_t sbox[32];
};
#endif /* mbc10 */

char *bt_cipher_name(void);
int bt_cipher_block(void);
int bt_cipher_key(void);
int bt_cipher_rounds(void);
void bt_cipher_prepare(void *, struct ci_key *);
void bt_cipher_encode(void *, struct ci_key *, int);
void bt_cipher_decode(void *, struct ci_key *, int);
void bt_cfb(u_int32_t *, int, struct ci_key *, u_int32_t *, int, int);
void bt_key(u_int32_t *, int, struct ci_key *);

#endif
